package impl;

import comm.Helper;

import entity.*;
import dao.*;

public class ResumeImpl {

	private IResumeDao resumeDao;

	public IResumeDao getResumeDao() {
		return resumeDao;
	}

	public void setResumeDao(IResumeDao resumeDao) {
		this.resumeDao = resumeDao;
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public ResumeEntity resumeUserId(int userid) {
		ResumeEntity info = this.resumeDao.resumeByUserId(userid);
		return info;
	}

	/**
	 * 详情
	 * 
	 * @param id
	 * @return
	 */
	public ResumeEntity info(int id) {
		ResumeEntity info = this.resumeDao.info(id);
		if(null != info){
			String date = info.getCreateTime();
			date = Helper.getShortDate3(date);
			info.setCreateTime(date);
			String content = info.getContent();
			content = content.replace("nowrap;", "normal;");
			info.setContent(content);
		}
		return info;
	}

	/**
	 * 添加
	 * 
	 * @param note
	 * @return
	 */
	public int add(ResumeEntity entity) {
		return this.resumeDao.add(entity);
	}

	/**
	 * 编辑
	 * 
	 * @param entity
	 * @return
	 */
	public int upt(ResumeEntity entity) {
		return this.resumeDao.upt(entity);
	}

	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchg(ResumeEntity entity) {
		String content = entity.getContent();
		if (content.length() > 0) {
			content = content.replaceAll("<[^>]+>", "");
		}
		if (content.length() > 100) {
			content = content.substring(0, 100);
			content = content + "...";
		}
		entity.setShortContent(content);
		String date = entity.getCreateTime();
		date = Helper.getShortDate3(date);
		entity.setCreateTime(date);
	}
}
