package impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import comm.Helper;

import entity.*;
import dao.*;

public class NoteImpl {

	private INoteDao noteDao = null;

	public INoteDao getNoteDao() {
		return noteDao;
	}

	public void setNoteDao(INoteDao noteDao) {
		this.noteDao = noteDao;
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public int cnt(int usrId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		return this.noteDao.listCnt(map);
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public List<NoteEntity> list(int usrId, int page, int size) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		map.put("start", start);
		map.put("size", size);
		List<NoteEntity> list = this.noteDao.list(map);
		if (null != list)
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i));
			}
		return list;
	}

	/**
	 * 详情
	 * 
	 * @param id
	 * @return
	 */
	public NoteEntity info(int id) {
		NoteEntity note = this.noteDao.info(id);
		String date = note.getAt();
		date = Helper.getShortDate3(date);
		note.setAt(date);
		String content = note.getContent();
		content = content.replace("nowrap;", "normal;");
		note.setContent(content);
		return note;
	}

	/**
	 * 添加
	 * 
	 * @param note
	 * @return
	 */
	public int add(NoteEntity note) {
		return this.noteDao.add(note);
	}

	/**
	 * 编辑
	 * 
	 * @param note
	 * @return
	 */
	public int upt(NoteEntity note) {
		return this.noteDao.upt(note);
	}

	/**
	 * 删除
	 * 
	 * @param loginId
	 * @param noteId
	 * @return
	 * 
	 */
	public int del(int loginId, int noteId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", loginId);
		map.put("id", noteId);
		return this.noteDao.del(map);
	}

	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchg(NoteEntity entity) {
		String content = entity.getContent();
		if (content.length() > 0) {
			content = content.replaceAll("<[^>]+>", "");
		}
		if (content.length() > 100) {
			content = content.substring(0, 100);
			content = content + "...";
		}
		entity.setShortContent(content);
		String date = entity.getAt();
		date = Helper.getShortDate3(date);
		entity.setAt(date);
	}
}
