package impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import comm.Helper;

import entity.*;
import dao.*;

public class NewsImpl {

	private INewsDao newsDao = null;

	public INewsDao getNewsDao() {
		return newsDao;
	}

	public void setNewsDao(INewsDao newsDao) {
		this.newsDao = newsDao;
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public int cnt(int usrId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		return this.newsDao.listCnt(map);
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public List<NewsEntity> list(int usrId, int page, int size) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		map.put("start", start);
		map.put("size", size);
		List<NewsEntity> list = this.newsDao.list(map);
		if (null != list)
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i));
			}
		return list;
	}

	/**
	 * 详情
	 * 
	 * @param id
	 * @return
	 */
	public NewsEntity info(int id) {
		NewsEntity news = this.newsDao.info(id);
		String date = news.getAt();
		date = Helper.getShortDate3(date);
		news.setAt(date);
		String content = news.getContent();
		content = content.replace("nowrap;", "normal;");
		news.setContent(content);
		return news;
	}

	/**
	 * 添加
	 * 
	 * @param news
	 * @return
	 */
	public int add(NewsEntity news) {
		return this.newsDao.add(news);
	}

	/**
	 * 编辑
	 * 
	 * @param news
	 * @return
	 */
	public int upt(NewsEntity news) {
		return this.newsDao.upt(news);
	}

	/**
	 * 删除
	 * 
	 * @param loginId
	 * @param newsId
	 * @return
	 * 
	 */
	public int del(int loginId, int newId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", loginId);
		map.put("id", newId);
		return this.newsDao.del(map);
	}

	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchg(NewsEntity entity) {
		String content = entity.getContent();
		if (content.length() > 0) {
			content = content.replaceAll("<[^>]+>", "");
		}
		if (content.length() > 100) {
			content = content.substring(0, 100);
			content = content + "...";
		}
		entity.setShortContent(content);
		String date = entity.getAt();
		date = Helper.getShortDate3(date);
		entity.setAt(date);
	}
}
