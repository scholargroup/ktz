package impl;

import dao.ILogDao;
import entity.LogEntity;

public class LogImpl {

	public ILogDao getLogDao() {
		return logDao;
	}

	public void setLogDao(ILogDao logDao) {
		this.logDao = logDao;
	}

	/**
	 * 
	 */
	private ILogDao logDao=null;
	
	/**
	 * 
	 * @param entity
	 * @return
	 */
	public int add(LogEntity entity)
	{
		return this.logDao.add(entity);
	}
}
