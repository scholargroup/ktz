package impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import comm.Helper;

import entity.*;
import dao.*;

public class JobImpl {

	private IJobDao jobDao = null;


	public IJobDao getJobDao() {
		return jobDao;
	}

	public void setJobDao(IJobDao jobDao) {
		this.jobDao = jobDao;
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public int cnt(int usrId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		return this.jobDao.listCnt(map);
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public List<JobEntity> list(int usrId, int page, int size, int length) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		map.put("start", start);
		map.put("size", size);
		List<JobEntity> list = this.jobDao.list(map);
		if (null != list)
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i), length);
			}
		return list;
	}

	/**
	 * 详情
	 * 
	 * @param id
	 * @return
	 */
	public JobEntity info(int id) {
		JobEntity job = this.jobDao.info(id);
		String date = job.getAt();
		date = Helper.getShortDate3(date);
		job.setAt(date);
		String desc = job.getJobDesc();
		desc = desc.replace("nowrap;", "normal;");
		job.setJobDesc(desc);
		return job;
	}

	/**
	 * 添加
	 * 
	 * @param job
	 * @return
	 */
	public int add(JobEntity job) {
		return this.jobDao.add(job);
	}

	/**
	 * 编辑
	 * 
	 * @param note
	 * @return
	 */
	public int upt(JobEntity job) {
		return this.jobDao.upt(job);
	}

	/**
	 * 删除
	 * 
	 * @param loginId
	 * @param noteId
	 * @return
	 * 
	 */
	public int del(int loginId, int jobId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", loginId);
		map.put("id", jobId);
		return this.jobDao.del(map);
	}

	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchg(JobEntity entity, int length) {
		String content = entity.getJobDesc();
		if (content !=null && content.length() > 0) {
			content = content.replaceAll("<[^>]+>", "");
		}
		if (content !=null && content.length() > length) {
			content = content.substring(0, length);
			content = content + "...";
		}
		entity.setShortContent(content);
		String date = entity.getAt();
		date = Helper.getShortDate3(date);
		entity.setAt(date);
	}
}
