package impl;

import java.util.Map;

import entity.*;
import dao.*;

public class FollowImpl {

	
	private IFollowDao followDao = null;


	public IFollowDao getFollowDao() {
		return followDao;
	}

	public void setFollowDao(IFollowDao followDao) {
		this.followDao = followDao;
	}

	/**
	 * 数量
	 * 
	 * @return
	 */
	public int cnt(Map<String, String> map) {
		return this.followDao.listFollowCnt(map);
	}

	/**
	 * 添加
	 * 
	 * @param job
	 * @return
	 */
	public int add(FollowEntity entity) {
		return this.followDao.add(entity);
	}

	/**
	 * 删除
	 * 
	 * @param loginId
	 * @param noteId
	 * @return
	 * 
	 */
	public int del(FollowEntity entity) {
		return this.followDao.del(entity);
	}
}
