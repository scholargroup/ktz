package action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import comm.Helper;
import entity.JsonEntity;
import entity.ResearchGroupEntity;
import impl.RGImpl;

/**
 * 
 */
public class RGAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(RGAction.class);

	private RGImpl rgService = null;
	private String title = "";
	private String content = "";
	private String url = ""; //< 详细URL
	private String area = ""; //< 领域
	private List<ResearchGroupEntity> list = null;
	private ResearchGroupEntity info = null;

//	/**
//	 * 列表，没有分页
//	 * 
//	 * @return
//	 */
//	public String list() {
//		int loginId = super.getLoginId();
//		int page = Helper.parseInt(super.getP());
//		page = page < 1 ? 1 : page;
//		super.setAttr("page", page);
//		super.setAttr("size", size);
//		int cnt = this.rgService.cnt(loginId);
//		super.setAttr("record", cnt);
//		List<ResearchGroupEntity> list = null;
//		if (0 < cnt) {
//			list = this.rgService.list(loginId, page, size);
//			setList(list);
//		}
//		return "list";
//	}
//
	/**
	 * 详情
	 * 
	 * @return
	 */
	public String info() {
		int id = super.getLoginId();
		ResearchGroupEntity rg = this.rgService.info(id);
		setInfo(rg);
		return "info";
	}

	/**
	 * 
	 * @return
	 */
	public String add() {
		if (super.getLoginId() == 0) {
			super.setMessage("无权访问!");
		}
		return "add";
	}

	/**
	 * 
	 * @return
	 */
	public String save() {
		int loginId = super.getLoginId();
		int id =this.rgService.getId(loginId);
		
		if (title.equals("")) {
			title = "课题组_"
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		}

		ResearchGroupEntity rg = new ResearchGroupEntity();
		rg.setId(id);
		rg.setCategoryId(0);
		rg.setTitle(getTitle());
		rg.setContent(getContent());
		rg.setDetailUrl(getUrl());
		rg.setResearchArea(getArea());
		rg.setUsrId(super.getLoginId());

		// 更新
		if (id > 0) {
			int ret = this.rgService.upt(rg);
			if (ret > 0) {
				super.setMessage("1");
				return "save-success";
			} else {
				super.setMessage("保存失败 !");
			}
			return "info";
		} else {
			if (super.getLoginId() == 0) {
				super.setMessage("无权访问!");
			} else {
				// 自动生成Id
				rg.setId(super.getAutoId());
				int ret = this.rgService.add(rg);
				if (ret > 0) {
					super.setMessage("1");
					return "save-success";
				}
				super.setMessage("保存失败 !");
			}
			return "info";
		}
	}

	public RGImpl getRgService() {
		return rgService;
	}

	public void setRgService(RGImpl rgService) {
		this.rgService = rgService;
	}

	/**
	 * 
	 * @return
	 */
	public String del() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);
		String sId = super.getId();
		int id = Helper.parseInt(sId);
		int loginId = super.getLoginId();
		// 成功
		if (id > 0 && loginId > 0) {
			this.rgService.del(loginId, id);
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}

	public String getTitle() {
		if (title == null) {
			title = "";
		}
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		if (content == null) {
			content = "";
		}
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public List<ResearchGroupEntity> getList() {
		return list;
	}

	public void setList(List<ResearchGroupEntity> list) {
		this.list = list;
	}

	public ResearchGroupEntity getInfo() {
		return info;
	}

	public void setInfo(ResearchGroupEntity info) {
		this.info = info;
	}
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

}