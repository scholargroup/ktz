package action;

import java.util.List;

import org.apache.log4j.Logger;

import comm.Constant;
import comm.Helper;
import entity.FriendEntity;
import entity.JsonEntity;
import entity.UserEntity;
import impl.FridImpl;
import impl.MessageImpl;

/**
 * 
 */
public class FridAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(FridAction.class);

	private FridImpl fridService = null;

	private List<FriendEntity> list = null;
	private List<UserEntity> usrList = null;
	private FriendEntity info = null;
	
	private MessageImpl messageService;
	
	private int senderId;

	/**
	 * 列表
	 * 
	 * @return
	 */
	public String list() {
		int userId = super.getLoginId();
		super.setAuthorize("1");
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.fridService.cnt(userId);
		super.setAttr("record", cnt);
		List<FriendEntity> list = null;
		if (0 < cnt) {
			list = this.fridService.list(userId, page, size, host);
			setList(list);
		}
		return "list";
	}

	/**
	 * top
	 * 
	 * @return
	 */
	public String top() {
		int usrId = getLoginId();
		super.setId("" + usrId);
		List<FriendEntity> noteList = this.fridService.top(usrId, host);
		setList(noteList);
		return "top";
	}

	/**
	 * 
	 * @return
	 */
	public String add() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);

		String id = super.getId();
		int msgId = Helper.parseInt(id);
		int loginId = super.getLoginId();

		int ret = this.fridService.add(super.getAutoId(), loginId, loginId, senderId);
		
		messageService.uptmMsgCategory(msgId, loginId, Constant.MESSAGE_FRIENDREQ);
		if (ret > 0) {
			json.setStatus(ret);
		}
		super.json(json);
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public String del() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);
		String sId = super.getId();
		int id = Helper.parseInt(sId);
		int loginId = super.getLoginId();
		// 成功
		if (id > 0 && loginId > 0) {
			this.fridService.del(id, loginId);
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}

	public FriendEntity getInfo() {
		return info;
	}

	public void setInfo(FriendEntity info) {
		this.info = info;
	}

	public FridImpl getFridService() {
		return fridService;
	}

	public void setFridService(FridImpl fridService) {
		this.fridService = fridService;
	}

	public List<FriendEntity> getList() {
		return list;
	}

	public void setList(List<FriendEntity> list) {
		this.list = list;
	}

	public List<UserEntity> getUsrList() {
		return usrList;
	}

	public void setUsrList(List<UserEntity> usrList) {
		this.usrList = usrList;
	}

	public MessageImpl getMessageService() {
		return messageService;
	}

	public void setMessageService(MessageImpl messageService) {
		this.messageService = messageService;
	}

	public int getSenderId() {
		return senderId;
	}

	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}
}