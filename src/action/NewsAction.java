package action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import comm.Helper;
import entity.JsonEntity;
import entity.NewsEntity;
import entity.NoteEntity;
import impl.NewsImpl;

/**
 * 
 */
public class NewsAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(NewsAction.class);

	private NewsImpl newsService = null;

	private String title = "";
	private String content = "";
	private List<NewsEntity> list = null;
	private NewsEntity info = null;

	/**
	 * 个人列表新闻，没有分页
	 * 
	 * @return
	 */
	public String list() {
		int loginId = super.getUserId();
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.newsService.cnt(loginId);
		super.setAttr("record", cnt);
		List<NewsEntity> newList = null;
		if (0 < cnt) {
			newList = this.newsService.list(loginId, page, size);
			setList(newList);
		}
		return "list";
	}
	
	/**
	 * 所有用户新闻，没有分页
	 * 
	 * @return
	 */
	public String allList() {
		int loginId = 0; //登陆ID为0，则代表查询所有
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.newsService.cnt(loginId);
		super.setAttr("record", cnt);
		List<NewsEntity> newList = null;
		if (0 < cnt) {
			newList = this.newsService.list(loginId, page, size);
			setList(newList);
		}
		return "allList";
	}

	/**
	 * top
	 * 
	 * @return
	 */
	public String top() {
		int usrId = getUserId();
		super.setId(""+usrId);
		list = this.newsService.list(usrId, 1, 10);
		return "top";
	}

	/**
	 * 详情
	 * 
	 * @return
	 */
	public String info() {
		String sId = this.getId();
		int id = Helper.parseInt(sId);
		setId("" + id);

		NewsEntity note = this.newsService.info(id);
		setInfo(note);
		return "info";
	}

	/**
	 * 
	 * @return
	 */
	public String add() {
		if (super.getLoginId() == 0) {
			super.setMessage("无权访问!");
		}
		return "add";
	}

	/**
	 * 
	 * @return
	 */
	public String save() {
		String sId = this.getId();
		int id = Helper.parseInt(sId);

		if (title.equals("")) {
			title = "新闻_"
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		}

		NewsEntity note = new NewsEntity();
		note.setId(id);
		note.setCategoryId(0);
		note.setTitle(getTitle());
		note.setContent(getContent());
		note.setUsrId(super.getLoginId());

		// 更新
		if (id > 0) {
			int ret = this.newsService.upt(note);
			if (ret > 0) {
				super.setMessage("1");
			} else {
				super.setMessage("保存失败 !");
			}
			return "edit";
		} else {
			if (super.getLoginId() == 0) {
				super.setMessage("无权访问!");
			} else {
				// 自动生成Id
				note.setId(super.getAutoId());
				int ret = this.newsService.add(note);
				if (ret > 0) {
					super.setMessage("1");
					return "save-success";
				}
				super.setMessage("保存失败 !");
			}
			return "add";
		}
	}

	/**
	 * 
	 * @return
	 */
	public String del() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);
		String sId = super.getId();
		int id = Helper.parseInt(sId);
		int loginId = super.getLoginId();
		// 成功
		if (id > 0 && loginId > 0) {
			this.newsService.del(loginId, id);
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}

	public String getTitle() {
		if (title == null) {
			title = "";
		}
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		if (content == null) {
			content = "";
		}
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<NewsEntity> getList() {
		return list;
	}

	public void setList(List<NewsEntity> list) {
		this.list = list;
	}

	public NewsEntity getInfo() {
		return info;
	}

	public void setInfo(NewsEntity info) {
		this.info = info;
	}

	public NewsImpl getNewsService() {
		return newsService;
	}

	public void setNewsService(NewsImpl newsService) {
		this.newsService = newsService;
	}
}