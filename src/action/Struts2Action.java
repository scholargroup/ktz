package action;

import impl.AppContext;
import impl.UserImpl;

import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.CookiesAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import comm.ConfigHelper;
import comm.Encrypt;
import comm.Helper;
import comm.Jsoner;
import comm.SimpleMailEntity;
import comm.SimpleMailSender;
import entity.UserEntity;

public class Struts2Action extends ActionSupport implements SessionAware,
		CookiesAware {

	protected static Logger logger = Logger.getLogger(Struts2Action.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static int sysId = 0;

	private String id = "";
	private String message = "";
	protected int size = 10;
	static String path = "";
	static String host = "";
	static String filePath = "";
	static String fileHost = "";
	private String authorize = ""; // /< 访问权限，如果authorize=1,表示有权限，其他拒绝。

	static {
		if (path.equals("")) {
			path = ConfigHelper.getValue("img_path");
		}
		if (host.equals("")) {
			host = ConfigHelper.getValue("img_host");
		}
		if (filePath.equals("")) {
			filePath = ConfigHelper.getValue("file_path");
		}
		if (fileHost.equals("")) {
			fileHost = ConfigHelper.getValue("file_host");
		}
	}

	/**
	 * page
	 */
	private String p = "";
	private String q = "";

	/**
	 * 
	 * @return
	 */
	protected synchronized static int getAutoId() {
		if (sysId == 0) {
			sysId = (int) (System.currentTimeMillis() / 1000) - 1378800000;
			if (sysId < 0) {
				sysId = 100000;
			}
		}
		sysId++;
		return sysId;
	}

	/**
	 * MD5
	 * 
	 * @param s
	 * @return
	 */
	protected String md5(String s) {
		s = s.trim();
		return Encrypt.md5(s + "111~123");
	}

	/**
	 * 
	 * @param usr_mail
	 */
	public void sendMail(String usr_mail) {
		// SimpleMailEntity sendInfo = new SimpleMailEntity();
		// sendInfo.setFormAddr("cocomba@163.com");
		// sendInfo.setFormName("乐乎网");
		// sendInfo.setSmtpHost("smtp.163.com");
		// sendInfo.setSmtpPort("25");
		// sendInfo.setSmtpValidate(true);
		// sendInfo.setUserName("cocomba@163.com");
		// sendInfo.setUserPwd("871226hanrui");
		//
		// SimpleMailSender sender = new SimpleMailSender(sendInfo);
		// String title = "欢迎来到乐乎的世界（wolehu.com），请验证您的账号";
		//
		// String vc = usr_mail + "@@@@@" + System.currentTimeMillis();
		// try {
		// vc = Encrypt.desEncrypt(vc);
		// String link = "http://www.wolehu.com/verify_email?vc=" + vc;
		// String content = getMailContent(usr_mail, link);
		// sender.sendHtmlMail(usr_mail, title, content);
		// } catch (Exception e) {
		// logger.error(e);
		// }
	}

	/**
	 * 
	 * @param cls
	 */
	public void json(Object cls) {
		ServletActionContext.getRequest().setAttribute("Accept",
				"Accept:application/json, text/javascript, */*; q=0.01");

		ServletActionContext.getResponse().setCharacterEncoding("utf-8");
		ServletActionContext.getResponse().setContentType("application/json");
		try {

			String jsonString = Jsoner.getJsonString(cls);

			PrintWriter pWriter = ServletActionContext.getResponse()
					.getWriter();
			pWriter.write(jsonString);
			pWriter.flush();
			pWriter.close();
		} catch (Exception e) {
			logger.error(e);
		}
	}

	/**
	 * 获取Session
	 * 
	 * @return
	 */
	public UserEntity getSession() {
		Map<String, Object> session = ActionContext.getContext().getSession();
		if (session != null) {
			UserEntity user = (UserEntity) session.get("user_info");
			return user;
		}
		return null;
	}

	/**
	 * 更新Session
	 */
	public void uptSession() {
		int loginId = getLoginId();
		if (loginId > 0) {
			uptSession(loginId);
		}
	}

	/**
	 * 
	 * @return 0 未登录用户
	 */
	public int getLoginId() {
		UserEntity entity = getSession();
		if (entity != null) {
			return entity.getId();
		}
		return 0;
	}

	/**
	 * 获取用户Id，主要是用于用户主页等
	 * 
	 * @return
	 */
	public int getUserId() {
		int userId = Helper.parseInt(getId());
		if (0 >= userId) {
			userId = getLoginId();
		}
		if (userId == getLoginId()) {
			this.authorize = "1";
		}
		return userId;
	}

	/**
	 * IP
	 * 
	 * @return
	 */
	protected String getIP() {
		HttpServletRequest request = ServletActionContext.getRequest();
		String ip = request.getHeader("x-forwarded-for");
		if (!bIP(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (!bIP(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (!bIP(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * check IP
	 * 
	 * @param ip
	 * @return
	 */
	private static boolean bIP(String ip) {
		if (ip == null || ip.length() == 0 || "unkown".equalsIgnoreCase(ip)
				|| ip.split(".").length != 4) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param key
	 * @param obj
	 */
	public void setAttr(String key, Object obj) {
		ServletActionContext.getRequest().setAttribute(key, obj);
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	String trim(String s) {
		if (s == null) {
			s = "";
		} else {
			s = s.trim();
		}
		return s;
	}

	/**
	 * Cookies
	 */
	public void setCookiesMap(Map<String, String> arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * Session
	 */
	public void setSession(Map<String, Object> arg0) {

	}

	/**
	 * 
	 * @param loginId
	 */
	protected UserEntity uptSession(int loginId) {
		if (loginId > 0) {
			UserImpl impl = (UserImpl) AppContext.getInstance().getBean(
					"userService");
			UserEntity user = impl.info(loginId, host);
			Map<String, Object> session = ActionContext.getContext()
					.getSession();
			session.put("user_info", user);
			return user;
		}
		return null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getP() {
		return p;
	}

	public void setP(String p) {
		this.p = p;
	}

	public long getRoleId() {
		UserEntity entity = getSession();
		if (entity != null) {
			return entity.getUsrRole();
		}
		return 0;
	}

	public String getQ() {
		return q;
	}

	public void setQ(String q) {
		this.q = q;
	}

	/**
	 * 
	 * @param address
	 * @param link
	 * @return
	 */
	public static String getMailContent(String address, String link) {
		String htm = "<div style=\"width:600px;margin:auto;line-height:2;font-size:12px;overflow:hidden;border-radius:3px;background:#FFF;box-shadow:0 0 10px rgba(0, 0, 0, 0.2);\">"
				+ "<div style=\"padding:15px 30px;word-wrap:break-word;border:solid #C5C5C5;border-width:0 1px;\">"
				// 
				+ "<br>"
				+ "<div style=\"line-height:28px;font-size:14px;font-weight:bold;\">您好！感谢您注册乐乎网。</div>"
				+ "<p>你注册的邮箱为："
				+ address
				+ "</p>"
				+ "<p>为激活账号，请单击以下链接验证您的账号。</p>"
				+ "<p><a href=\""
				+ link
				+ "\" target=\"_blank\">点击此处立即验证你的帐号</a>"
				+ "<span style=\"color:#999;\">(该链接在24小时内有效)</span></p>"
				+ "<br>"
				+ "<p style=\"color:#999;\">此信是由 <a href=\"http://www.wolehu.com\" target=\"_blank\">乐乎网</a> 系统发出，系统不接收回信，请勿直接回复。</p>	"
				+ "</div>" + "</div>";
		return htm;
	}

	public String getAuthorize() {
		return authorize;
	}

	public void setAuthorize(String authorize) {
		this.authorize = authorize;
	}
}
