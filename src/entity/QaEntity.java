package entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QaEntity {
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUsrId() {
		return usrId;
	}

	public void setUsrId(int usrId) {
		this.usrId = usrId;
	}

	public String getUsrName() {
		return usrName;
	}

	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getVisitCnt() {
		return visitCnt;
	}

	public void setVisitCnt(int visitCnt) {
		this.visitCnt = visitCnt;
	}

	public String getAt() {
		return at;
	}

	public void setAt(String at) {
		this.at = at;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getShortContent() {
		return shortContent;
	}

	public void setShortContent(String shortContent) {
		this.shortContent = shortContent;
	}

	public int getAnswerCnt() {
		return answerCnt;
	}

	public void setAnswerCnt(int answerCnt) {
		this.answerCnt = answerCnt;
	}

	public int getFollowId() {
		return followId;
	}

	public void setFollowId(int followId) {
		this.followId = followId;
	}

	public String getUniversityString() {
		return universityString;
	}

	public void setUniversityString(String universityString) {
		this.universityString = universityString;
	}

	public String getUniversityString2() {
		return universityString2;
	}

	public void setUniversityString2(String universityString2) {
		this.universityString2 = universityString2;
	}

	public String getQaLabel() {
		return qaLabel;
	}

	public void setQaLabel(String qaLabel) {
		this.qaLabel = qaLabel;
	}

	public List<String> getQaLabelList() {
		if(qaLabel != null && !"".equals(qaLabel)){
			qaLabelList.clear();
			String[] arr = qaLabel.split("，");
			Collections.addAll(qaLabelList, arr);
		}
		return qaLabelList;
	}

	public void setQaLabelList(List<String> qaLabelList) {
		this.qaLabelList = qaLabelList;
	}

	public String getUsrAvatar() {
		return usrAvatar;
	}

	public void setUsrAvatar(String usrAvatar) {
		this.usrAvatar = usrAvatar;
	}

	public String getUsrAvatarUrl() {
		return usrAvatarUrl;
	}

	public void setUsrAvatarUrl(String usrAvatarUrl) {
		this.usrAvatarUrl = usrAvatarUrl;
	}

	public int getUsrRole() {
		return usrRole;
	}

	public void setUsrRole(int usrRole) {
		this.usrRole = usrRole;
	}

	private int id = 0;
	private int usrId = 0;
	private String usrName = "";
	private String usrAvatar = "";
	private String usrAvatarUrl = "";
	private int categoryId = 0;
	private String categoryName = "";
	private String title = "";
	private String content = "";
	private String shortContent = "";
	private int visitCnt = 0;
	private String at = "";
	private int answerCnt = 0;
	//当前登陆人如果关注了该文章的关注id
	private int followId;
	
	private String universityString;//发布人大学
	private String universityString2; //学院
	private String qaLabel; //问题标签
	private List<String> qaLabelList = new ArrayList<String>(); //标签集合
	/**
	 * 发布人类型
	 * 0：个人注册；  1：课题组注册
	 */
	private int usrRole = 0; 
}
