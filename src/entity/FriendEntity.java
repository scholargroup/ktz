package entity;

public class FriendEntity {
	private int id = 0;
	private int loginId = 0;
	private int usrId = 0;
	private int fridId = 0;
	private UserEntity friInfo = null;
	private int status = 0;
	private String remark = "";

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUsrId() {
		return usrId;
	}

	public void setUsrId(int usrId) {
		this.usrId = usrId;
	}

	public int getFridId() {
		return fridId;
	}

	public void setFridId(int fridId) {
		this.fridId = fridId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getLoginId() {
		return loginId;
	}

	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}

	public UserEntity getFriInfo() {
		return friInfo;
	}

	public void setFriInfo(UserEntity friInfo) {
		this.friInfo = friInfo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}