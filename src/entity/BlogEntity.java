package entity;

public class BlogEntity {
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUsrId() {
		return usrId;
	}

	public void setUsrId(int usrId) {
		this.usrId = usrId;
	}

	public String getUsrName() {
		return usrName;
	}

	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getVisitCnt() {
		return visitCnt;
	}

	public void setVisitCnt(int visitCnt) {
		this.visitCnt = visitCnt;
	}

	public String getAt() {
		return at;
	}

	public void setAt(String at) {
		this.at = at;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getShortContent() {
		return shortContent;
	}

	public void setShortContent(String shortContent) {
		this.shortContent = shortContent;
	}

	public int getUsrRole() {
		return usrRole;
	}

	public void setUsrRole(int usrRole) {
		this.usrRole = usrRole;
	}

	public String getUsrAvatarUrl() {
		return usrAvatarUrl;
	}

	public void setUsrAvatarUrl(String usrAvatarUrl) {
		this.usrAvatarUrl = usrAvatarUrl;
	}

	public String getUsrAvatar() {
		return usrAvatar;
	}

	public void setUsrAvatar(String usrAvatar) {
		this.usrAvatar = usrAvatar;
	}

	public String getUniversityString() {
		return universityString;
	}

	public void setUniversityString(String universityString) {
		this.universityString = universityString;
	}

	public String getUniversityString2() {
		return universityString2;
	}

	public void setUniversityString2(String universityString2) {
		this.universityString2 = universityString2;
	}

	public int getAnswerCnt() {
		return answerCnt;
	}

	public void setAnswerCnt(int answerCnt) {
		this.answerCnt = answerCnt;
	}

	private int id = 0;
	private int usrId = 0;
	private String usrName = "";
	private int categoryId = 0;
	private String categoryName = "";
	private String title = "";
	private String content = "";
	private String shortContent = "";
	private int visitCnt = 0;
	private String at = "";
	/**
	 * 发布人类型
	 * 0：个人注册；  1：课题组注册
	 */
	private int usrRole = 0; 
	
	private String usrAvatarUrl = "";
	private String usrAvatar = "";
	private String universityString;//发布人大学
	private String universityString2; //学院
	private int answerCnt = 0;
}
