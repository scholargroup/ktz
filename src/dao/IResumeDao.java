package dao;

import entity.*;

public interface IResumeDao {
	
	public ResumeEntity resumeByUserId(int userid);
	
	public ResumeEntity info(int id);
	
	public int add(ResumeEntity entity);

	public int upt(ResumeEntity entity);
	
}
