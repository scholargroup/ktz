package dao;

import java.util.List;
import java.util.Map;

import entity.*;

public interface IQADao {

	public int add(QaEntity entity);

	public int upt(QaEntity entity);
	
	public int del(Map<String, Integer> map);

	public QaEntity info(int id);
	
	public List<QaEntity> list(Map<String, Integer> map);

	public int listCnt(Map<String, Integer> map);

	public List<QaEntity> top(Map<String, Integer> map);

	public List<QaEntity> search(Map<String, Integer> map);

	public int searchCnt(Map<String, Integer> map);
	
	public int addReply(ReplyEntity entity);
	
	public List<ReplyEntity> listReply(Map<String, Integer> map);
	
	public int listReplyCnt(Map<String, Integer> map);
	
	public List<QaEntity> myAnswerList(Map<String, Integer> map);

	public int myAnswerListCnt(Map<String, Integer> map);
}
