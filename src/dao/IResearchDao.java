package dao;

import entity.*;

public interface IResearchDao {
	
	public ResearchEntity searchByUserId(int userid);
	
	public ResearchEntity info(int id);
	
	public int add(ResearchEntity research);

	public int upt(ResearchEntity research);
	
}
