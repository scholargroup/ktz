package dao;

import java.util.List;
import java.util.Map;

import entity.*;

public interface IImageDao {
	
	public int add(ImageEntity entity);

	public int addDetail(ImageEntity entity);

	public List<ImageEntity> list(Map<String, Integer> map);
	
	public int cnt();
}
