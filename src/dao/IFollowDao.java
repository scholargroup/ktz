package dao;

import java.util.Map;

import entity.*;

public interface IFollowDao {
	
	public int add(FollowEntity entity);
	
	public int del(FollowEntity entity);
	
	//关注对象的数量
	public int listFollowCnt(Map<String,String> map);
	
}
