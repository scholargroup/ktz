package dao;

import java.util.List;
import java.util.Map;

import entity.*;

public interface IBlogDao {
	
	public List<BlogEntity> list(Map<String,Integer> map);
	
	public int listCnt(Map<String,Integer> map);
	
	public BlogEntity info(int id);
	
	public int add(BlogEntity note);

	public int upt(BlogEntity note);
	
	public int del(Map<String,Integer> map);
	
	public int addReply(ReplyEntity entity);
	
	public List<ReplyEntity> listReply(Map<String, Integer> map);
	
	public int listReplyCnt(Map<String, Integer> map);
}
