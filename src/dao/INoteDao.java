package dao;

import java.util.List;
import java.util.Map;

import entity.*;

public interface INoteDao {
	
	public List<NoteEntity> list(Map<String,Integer> map);
	
	public int listCnt(Map<String,Integer> map);
	
	public NoteEntity info(int id);
	
	public int add(NoteEntity note);

	public int upt(NoteEntity note);
	
	public int del(Map<String,Integer> map);
	
}
