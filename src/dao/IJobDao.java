package dao;

import java.util.List;
import java.util.Map;

import entity.*;

public interface IJobDao {
	
	public List<JobEntity> list(Map<String,Integer> map);
	
	public int listCnt(Map<String,Integer> map);
	
	public JobEntity info(int id);
	
	public int add(JobEntity job);

	public int upt(JobEntity job);
	
	public int del(Map<String,Integer> map);
	
}
