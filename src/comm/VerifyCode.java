package comm;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;
import javax.imageio.ImageIO;

/**
 * @author dzy 生成验证码图片
 */
public class VerifyCode {
	// 验证码图片中可以出现的字符集，可根据需要修改
	private char mapTable[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
			'9' };

	/**
	 * 功能:生成彩色验证码图片 参数width为生成图片的宽度,参数height为生成图片的高度,参数os为页面的输出流
	 */
	public String getCertPic(OutputStream os) {
		// if (width <= 0)
		int width = 75;
		// if (height <= 0)
		int height = 25;
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		// 获取图形上下文
		Graphics2D  g = (Graphics2D )image.getGraphics();
		// 设定背景色
		//g.setColor(new Color(235, 235, 235));
		g.setColor(new Color(255, 255, 255));
		g.fillRect(0, 0, width, height);
		// 画边框
		g.setColor(new Color(1, 102, 183));
		g.drawRect(0, 0, width - 1, height - 1);
		// 取随机产生的认证码
		String strEnsure = "";
		// 4代表4位验证码,如果要生成更多位的认证码,则加大数值
		for (int i = 0; i < 4; ++i) {
			strEnsure += mapTable[(int) (mapTable.length * Math.random())];
		}
		// 　　将认证码显示到图像中,如果要生成更多位的认证码,增加drawString语句
		
		g.setFont(new Font("serif", Font.BOLD, 22));
		Random rd = new Random();
		for (int i = 0; i < strEnsure.length(); i++) {
			if(i%2==0){g.setColor(new Color(255, 15, 93));}
			else
			{g.setColor(new Color(0, 176, 76));}
			String str = strEnsure.substring(i, 1 + i);
			int x = 20 * (i) + rd.nextInt(13);
			if (x > (width-10)) {
				x = width-10;
			}
			int y = 15 + rd.nextInt(10);
			if (y > (height-2)) {
				y = height-2;
			}
			g.drawString(str, x, y);
		}
		// 随机产生1个干扰线
		g.setColor(new Color(rd.nextInt(255), rd.nextInt(255), rd.nextInt(255)));
		g.setStroke(new BasicStroke(rd.nextFloat()*4));

		Random rand = new Random();
		for (int i = 0; i < 1; i++) {
			int x1 = rand.nextInt(width-40);
			int y1 = rand.nextInt(height-10);
			int x2 = rand.nextInt(width);
			int y2 = rand.nextInt(height);
			g.drawLine(x1, y1, x2, y2);
		}
		// 释放图形上下文
		g.dispose();
		try {
			// 输出图像到页面
			ImageIO.write(image, "JPEG", os);
		} catch (IOException e) {
			return "";
		}
		return strEnsure;
	}
}
