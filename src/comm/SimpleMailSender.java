package comm;

import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * 邮件发送类
 * 
 * 需要导入类：mail.jar
 * 
 * @author desmond 2013-11-15
 */
public class SimpleMailSender {
	
	SimpleMailEntity mail = null;

	public SimpleMailSender(SimpleMailEntity mail) {
		this.mail = mail;
	}

	/**
	 * 发送邮件
	 * @param toAddr
	 * @param subject
	 * @param content
	 * @return
	 */
	public boolean sendHtmlMail(String toAddr, String subject, String content) {

		if (mail != null) {
			try {
				MyAuthenticator authenticator = null;
				if (mail.isSmtpValidate()) {
					authenticator = new MyAuthenticator(mail.getUserName(),
							mail.getUserPwd());
				}

				Properties p = new Properties();
				p.put("mail.smtp.host", mail.getSmtpHost());
				p.put("mail.smtp.port", mail.getSmtpPort());
				p.put("mail.smtp.auth", mail.isSmtpValidate() ? "true"
						: "false");

				Session sendMailSession = Session.getDefaultInstance(p,
						authenticator);

				Message mailMessage = new MimeMessage(sendMailSession);
				Address form = new InternetAddress(mail.getFormAddr(),mail.getFormName());
				mailMessage.setFrom(form);
				Address to = new InternetAddress(toAddr);
				mailMessage.setRecipient(RecipientType.TO, to);
				mailMessage.setSentDate(new Date());
				mailMessage.setSubject(subject);
				Multipart mainPart = new MimeMultipart();
				BodyPart html = new MimeBodyPart();
				html.setContent(content, "text/html; charset=utf-8");
				mainPart.addBodyPart(html);
				mailMessage.setContent(mainPart);

				Transport.send(mailMessage);
				return true;

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		return false;
	}

	/**
	 * 
	 * @author Administrator
	 * 
	 */
	public class MyAuthenticator extends Authenticator {
		String userName = null;
		String password = null;

		public MyAuthenticator() {
		}

		public MyAuthenticator(String username, String password) {
			this.userName = username;
			this.password = password;
		}

		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(userName, password);
		}
	}
}
