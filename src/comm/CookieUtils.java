package comm;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

/**
 * cookie的增加、删除、查询
 */
public class CookieUtils {
	public static final String USER_COOKIE = "www.wdsd.com";

	/**
	 * 
	 * @param usrName
	 * @param usrPwd
	 * @return
	 */
	public static Cookie addCookie(String usrName, String usrPwd) {
		String v = "";
		try {
			v = Encrypt.desEncrypt(usrName + "," + usrPwd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Cookie cookie = new Cookie(USER_COOKIE, v);
		// System.out.println("添加cookie");
		cookie.setMaxAge(60 * 60 * 24 * 14);// cookie保存两周
		cookie.setPath("/");
		return cookie;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public Map<String, String> getCookie(HttpServletRequest request) {
		Map<String, String> map = null;
		Cookie[] cookies = request.getCookies();
		// System.out.println("cookies: " + cookies);
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				// System.out.println("cookie: " + cookie.getName());
				if (CookieUtils.USER_COOKIE.equals(cookie.getName())) {
					map = new HashMap<String, String>();
					String value = cookie.getValue();
					try {
						value = Encrypt.desDecrypt(value);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (StringUtils.isNotBlank(value)) {
						String[] split = value.split(",");
						String username = split[0];
						String password = split[1];
						// username = Encrypt.desDecrypt(username);
						// password = Encrypt.desDecrypt(password);
						map.put("usrname", username);
						map.put("password", password);
					}
				}
			}
		}

		return map;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public Cookie delCookie(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (CookieUtils.USER_COOKIE.equals(cookie.getName())) {
					cookie.setValue("");
					cookie.setMaxAge(0);
					cookie.setPath("/");
					return cookie;
				}
			}
		}
		return null;
	}
}
