//package comm;
//
//import java.awt.Color;
//import java.awt.image.BufferedImage;
//import java.io.ByteArrayInputStream;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.security.MessageDigest;
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.imageio.ImageIO;
//
//import com.sun.image.codec.jpeg.JPEGCodec;
//import com.sun.image.codec.jpeg.JPEGEncodeParam;
//import com.sun.image.codec.jpeg.JPEGImageEncoder;
//
//public class Imger {
//	static String path = "";
//	static String host = "";
//	static int imgId = 0;
//	static {
//		path = ConfigHelper.getValue("img_path");
//		host = ConfigHelper.getValue("img_host");
//	}
//
//	public String qcode(String url) {
//		String imgSize="q";
//		int id=Helper.getId();
//		String qPath =getFullName(id,imgSize,".png");
//		TwoDimensionCode handler = new TwoDimensionCode();
//		handler.encoderQRCode(url, qPath, "png");
//		
//		String imgUrl = host + "/" + imgSize + "/"
//				+ getFolder(id) + "/" + getName(id,".png");
//		return imgUrl;
//	}
//	
//	/**
//	 * 头像
//	 * 
//	 * @param imgId
//	 * @return
//	 */
//	public static String get(int imgId) {
//		return get(1, 1, imgId);
//	}
//
//	/**
//	 * 获取图片的路径
//	 * 
//	 * @param type
//	 *            1 头像 0其他；
//	 * @param imgId
//	 * @param size
//	 *            1 小图 3大图 0原图
//	 * @return
//	 */
//	public static String get(int type, int size, int imgId) {
//		String url = "";
//		String sizeFolder = "s";
//		int folder = 0;
//		String imgName = "";
//		if (size == 3) {
//			sizeFolder = "l";
//		} else if (size == 0) {
//			sizeFolder = "o";
//		}
//
//		if (imgId < 10) {
//			if (type == 1) {
//				imgName = "10000.jpg";
//			}
//		} else {
//			folder = getFolder(imgId);
//			imgName = getName(imgId);
//		}
//		if (imgName.length() > 0) {
//			url = host + "/" + sizeFolder + "/" + folder + "/" + imgName;
//		}
//		return url;
//	}
//
//	// private String getUrl(int imgId) {
//	// int folder = getFolder(imgId);
//	// String imgname = getName(imgId);
//	// return "http://localhost:8080/img/o/" + folder + "/" + imgname;
//	// }
//
//	/**
//	 * 
//	 * @param path
//	 * @param host
//	 */
//	public Imger() {
//
//	}
//
//	/**
//	 * 裁剪
//	 * 
//	 * @param imgId
//	 * @param x
//	 * @param y
//	 * @param x2
//	 * @param y2
//	 * @return
//	 */
//	public int cut(int imgId, int x, int y, int w, int h) {
//		if (imgId > 0) {
//			int folder = getFolder(imgId);
//			String imgName = getName(imgId);
//			String fileName = path + "/o/" + folder + "/" + imgName;
//			File file = new File(fileName);
//			BufferedImage srcImage = null;
//			try {
//				int x2=x+w;
//				int y2=y+h;
//						
//				int sx = 0;
//				int sy = 0;
//				int sx2 = 0;
//				int sy2 = 0;
//				/* 读取图片信息 */
//				srcImage = ImageIO.read(file);
//				int sWidth = srcImage.getWidth();
//				int sHeight = srcImage.getHeight();
//				if (sWidth > 0 && sHeight > 0) {
//					int min = sWidth > sHeight ? sWidth : sHeight;
//					double rate = min / 300.0;
//					sx = (int) (x * rate);
//					sy = (int) (y * rate);
//					sx2 = (int) (x2 * rate);
//					sy2 = (int) (y2 * rate);
//					//if (x != 0 || y != 0 || x2 != 300 || y2 != 300) {
//						// byte[] bytes=ImageIO.write();
//						ByteArrayOutputStream out = new ByteArrayOutputStream();
//						try {
//							String format = imgName.substring(imgName
//									.lastIndexOf(".") + 1);
//							// System.out.println(">>>" + format);
//							ImageIO.write(srcImage, format, out);
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//						byte[] bytes = out.toByteArray();
////						doCompress(bytes,
////								path + "/s/" + folder + "/" + imgName, 48, 48,6
////								sx, sy, sx2, sy2);
////						doCompress(bytes,
////								path + "/l/" + folder + "/" + imgName, 100,
////								100, sx, sy, sx2, sy2);
//						
//						doCompress(bytes,
//								path + "/s/" + folder + "/" + imgName, 80, 80,
//								sx, sy, sx2, sy2);
//						doCompress(bytes,
//								path + "/l/" + folder + "/" + imgName, 160,
//								160, sx, sy, sx2, sy2);
//								
//					//}
//				}
//
//			} catch (Exception e) {
//				e.printStackTrace();
//			} finally {
//				srcImage.flush();
//			}
//		}
//		return 0;
//	}
//
//	/**
//	 * 图片ID获取方式同USERID 图片上传完后立即做图片的压缩。 头像的裁剪会覆盖原压缩的图片。 所有的图片都将压缩成JPG格式 1 头像 0其他；
//	 * 
//	 * @param type
//	 * @param bytes
//	 * @return
//	 */
//	public Map<String, String> upload(int type, byte[] bytes) {
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("state", "0");
//		map.put("info", "");
//		map.put("id", "0");
//		map.put("url", "");
//		map.put("width", "0");
//		map.put("height", "0");
//		int id = getImgId();
//		try {
//			int[] attr = original(id, bytes);
//			small(id, type, bytes);
//			large(id, type, bytes);
//			map.put("state", "1");
//			map.put("id", "" + id);
//			if(type==2){map.put("url", get(1, 3, id));}else{
//			map.put("url", get(type, 0, id));}
//			map.put("width", "" + attr[1]);
//			map.put("height", "" + attr[2]);
//
//		} catch (Exception e) {
//			map.put("state", "-1");
//			map.put("info", e.toString());
//		}
//		return map;
//	}
//
//	private int[] original(int imgId, byte[] bytes) {
//		String path = getFullName(imgId, "o");
//		return doCompress(bytes, path, 0, 0, 0, 0, 0, 0);
//	}
//
//	/**
//	 * 
//	 * @param imgId
//	 * @param type
//	 * @param bytes
//	 * @return
//	 */
//	private int[] small(int imgId, int type, byte[] bytes) {
//		return small(imgId, type, bytes, 0, 0, 0, 0);
//	}
//
//	/**
//	 * 
//	 * @param imgId
//	 * @param type
//	 * @param bytes
//	 * @return
//	 */
//	private int[] small(int imgId, int type, byte[] bytes, int x, int y,
//			int x2, int y2) {
//		String path = getFullName(imgId, "s");
//		int width = 100;
//		int height = 0;
//		if (type == 1) {
//			//width = 48;
//			//height = 48;
//			width = 80;
//			height = 80;
//		}
//		return doCompress(bytes, path, width, height, x, y, x2, y2);
//	}
//
//	private int[] large(int imgId, int type, byte[] bytes) {
//		return large(imgId, type, bytes, 0, 0, 0, 0);
//	}
//
//	/**
//	 * 
//	 * @param imgId
//	 * @param type
//	 * @param bytes
//	 * @return
//	 */
//	private int[] large(int imgId, int type, byte[] bytes, int x, int y,
//			int x2, int y2) {
//		String path = getFullName(imgId, "l");
//		int width = 420;
//		int height = 0;
//		if (type == 1) {
//			//width = 100;
//			//height = 100;
//			width = 160;
//			height = 160;
//		}
//		return doCompress(bytes, path, width, height, x, y, x2, y2);
//	}
//
//	//
//	// private int[] doCompress(byte[] bytes, String imgName, int width,
//	// int height, int x, int y, int x2, int y2) {
//	// int[] attr = { 0, 0, 0 };
//	// try {
//	// /* 读取图片信息 */
//	// BufferedImage srcImage = null;
//	// ByteArrayInputStream imgStream = new ByteArrayInputStream(bytes);
//	//
//	// srcImage = ImageIO.read(imgStream);
//	// attr = doCompress(srcImage, path, width, height, x, y, x2, y2);
//	// } catch (Exception e) {
//	// return null;
//	// }
//	// return attr;
//	// }
//
//	/**
//	 * 
//	 * @param bytes
//	 * @param imgName
//	 * @param width
//	 * @param height
//	 * @return
//	 */
//	private int[] doCompress(byte[] bytes, String imgName, int width,
//			int height, int x, int y, int x2, int y2) {
//		int[] attr = { 0, 0, 0 };
//		if (bytes != null) {
//			BufferedImage srcImage = null;
//			try {
//				/* 读取图片信息 */
//				ByteArrayInputStream imgStream = new ByteArrayInputStream(bytes);
//				//
//				srcImage = ImageIO.read(imgStream);
//				int sWidth = srcImage.getWidth();
//				attr[1] = sWidth;
//				int sHeight = srcImage.getHeight();
//				attr[2] = sHeight;
//
//				int ow = sWidth;
//				int oh = sHeight;
//				// int x = 0;
//				// int y = 0;
//				int w = sWidth;
//				int h = sHeight;
//
//				if (height == 0) {
//					if (width > 0) {
//						// 等比宽图；
//						// 是否缩放 小图不缩放
//						if (sWidth > width) {
//							double rate = ((double) sWidth) / (double) width;
//							oh = (int) (((double) sHeight) / rate);
//							ow = width;
//						}
//					}
//				} else {
//					if (width > 0) {
//						// 头像的等比缩放
//						if (width == height) {
//							ow = width;
//							oh = height;
//
//							if (sWidth > sHeight) {
//								w = sHeight;
//							} else {
//								h = sWidth;
//							}
//
//						} else {
//							if (sWidth > width) {
//								double rate1 = ((double) sWidth)
//										/ (double) width;
//								double rate2 = ((double) sHeight)
//										/ (double) height;
//
//								double rate = rate1 > rate2 ? rate1 : rate2;
//
//								ow = (int) (((double) sWidth) / rate);
//								oh = (int) (((double) sHeight) / rate);
//							}
//						}
//					}
//				}
//				// 为等比缩放计算输出的图片宽度及高度
//				/* 宽高设定 */
//				BufferedImage tag = new BufferedImage(ow, oh,
//						BufferedImage.TYPE_INT_RGB);
//				tag.getGraphics().setColor(Color.white);
//				tag.getGraphics().fillRect(0, 0, ow, oh);
//				if (x2 == 0) {
//					x2 = w;
//				}
//				if (y2 == 0) {
//					y2 = h;
//				}
//
//				tag.getGraphics().drawImage(srcImage, 0, 0, ow, oh, x, y, (x2),
//						(y2), null);
//
//				FileOutputStream out = new FileOutputStream(imgName);
//				JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
//				JPEGEncodeParam jep = JPEGCodec.getDefaultJPEGEncodeParam(tag);
//				/* 压缩质量 */
//				jep.setQuality(1, true);
//				encoder.encode(tag, jep);
//				out.close();
//			} catch (Exception e) {
//				e.printStackTrace();
//			} finally {
//				srcImage.flush();
//			}
//			attr[0] = 1;
//		}
//		return attr;
//	}
//
//	/**
//	 * 
//	 * @param imgId
//	 * @param imgSize
//	 * @return
//	 */
//	private String getFullName(int imgId, String imgSize,String ext) {
//		String folderPath = path + "/" + imgSize + "/" + getFolder(imgId);
//		createDir(folderPath);
//		String objName = folderPath + "/" + getName(imgId,ext);
//		return objName;
//	}
//	
//	/**
//	 * 
//	 * @param imgId
//	 * @param imgSize
//	 * @return
//	 */
//	private String getFullName(int imgId, String imgSize) {
//		return getFullName(imgId,  imgSize,"");
//	}
//
//	/**
//	 * 
//	 * @param imgId
//	 * @param type
//	 * @return
//	 */
//	public String getUrl(int imgId, int imgSize) {
//		String imgUrl = host + "/" + getTypeFolder(imgSize) + "/"
//				+ getFolder(imgId) + "/" + getName(imgId);
//		return imgUrl;
//	}
//
//	/**
//	 * 
//	 * @param path
//	 */
//	private void createDir(String path) {
//		File file = new File(path);
//		if (!file.isDirectory()) {
//			file.mkdirs();
//		}
//	}
//
//	/**
//	 * 
//	 * @param type
//	 *            1 samll 2 middle 3 large 0 origal
//	 * @return
//	 */
//	private String getTypeFolder(int type) {
//		String folder = "s";
//		if (type == 9) {
//			folder = "l";
//		}
//		return folder;
//	}
//
//	/**
//	 * 
//	 * @param imgId
//	 * @return
//	 */
//	private static int getFolder(int imgId) {
//		int folder = 0;
//		folder = imgId / 3000 + 1;
//		return folder;
//	}
//
//	/**
//	 * 
//	 * @param imgId
//	 * @return
//	 */
//	private static String getName(int imgId,String ext) {
//		String imgName = md5("" + imgId) + Integer.toHexString(imgId);
//		imgName = imgName.substring(imgName.length() - 12, imgName.length());
//		if(ext.equals("")){
//			ext=".jpg";
//		}
//		if (imgName.length() > 0) {
//			imgName +=ext;
//		}
//		return imgName;
//	}
//	
//	/**
//	 * 
//	 * @param imgId
//	 * @return
//	 */
//	private static String getName(int imgId) {
//		return getName(imgId, ".jpg");
//	}
//
//	/**
//	 * 
//	 * @param str
//	 * @return
//	 */
//	private static String md5(String str) {
//		// 返回字符串
//		String md5Str = null;
//		try {
//			// 操作字符串
//			StringBuffer buf = new StringBuffer();
//			MessageDigest md = MessageDigest.getInstance("MD5");
//			md.update(str.getBytes());
//			// 计算出摘要,完成哈希计算。
//			byte b[] = md.digest();
//			int i;
//			for (int offset = 0; offset < b.length; offset++) {
//				i = b[offset];
//				if (i < 0) {
//					i += 256;
//				}
//				if (i < 16) {
//					buf.append("0");
//				}
//				// 将整型 十进制 i 转换为16位，用十六进制参数表示的无符号整数值的字符串表示形式。
//				//
//				buf.append(Integer.toHexString(i));
//			}
//			// 32位的加密
//			md5Str = buf.toString();
//			// 16位的加密
//			md5Str = buf.toString().substring(8, 24);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return md5Str;
//	}
//
//	/**
//	 * 
//	 * @return
//	 */
//	private synchronized static int getImgId() {
//		if (imgId == 0) {
//			imgId = (int) (System.currentTimeMillis() / 1000) - 1378800000;
//			if (imgId < 0) {
//				imgId = 0;
//			}
//		}
//		imgId++;
//		return imgId;
//	}
//}
