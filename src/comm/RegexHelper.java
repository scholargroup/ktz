package comm;

import java.util.regex.Pattern;

public class RegexHelper {

	/**
	 * 邮箱
	 * @param input
	 * @return
	 */
	public static boolean email(String input){
		String regex="^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
		return matcher(regex,input);
	}
	
	/**
	 * 真实姓名
	 * @param input
	 * @return
	 */
	public static boolean usrName(String input){
		String regex="^[\u4e00-\u9fa5]{2,10}|[a-z.A-Z]{2,10}$";
		return matcher(regex,input);
	} 
	
	/**
	 * 手机号码
	 * @param input
	 * @return
	 */
	public static boolean mobile(String input){
		String regex="^13[0-9]{9}$|14[0-9]{9}$|15[0-9]{9}$|18[0-9]{9}$";
		return matcher(regex,input);
	}
	
	public static boolean tel(String input){
		String regex = "^[0-9]{3,4}[-/]{0,1}[0-9]{7,8}$|[0-9]{7,8}$|13[0-9]{9}$|14[0-9]{9}|15[0-9]{9}$|18[0-9]{9}$";
		return matcher(regex,input);
	}
	
	public static boolean qq(String input){
		String regex = "^[1-9]{1}[0-9]{3,10}$";
		return matcher(regex,input);
	}
	
	public static boolean pwd(String input){
		String regex = "^[\\w\\W]{6,16}$";
		return matcher(regex,input);
	}
	
	/**
	 * 
	 * @param regex
	 * @param input
	 * @return
	 */
	private static boolean matcher(String regex,String input){
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(input).matches();
		
	}
}
