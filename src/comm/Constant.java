package comm;

public class Constant {

	//关注时不同的类型
	/**
	 * 问题关注
	 */
	public static String QA = "QA";
	
	/**
	 * 用户关注
	 */
	public static String USER = "USER";
	
	/**
	 * 课题组组长标识
	 */
	public static Integer HEADMAN = 1;
	
	/**
	 * 课题组合作老师标识
	 */
	public static Integer TEACHERMAN = 2;
	
	/**
	 * 课题组合作学生标识
	 */
	public static Integer STUDENTMAN = 3;
	
	/**
	 * 普通消息
	 */
	public static int MESSAGE_COMMON = 0;
	
	/**
	 * 好友请求消息
	 */
	public static int MESSAGE_FRIENDREQ = 1;
	
	/**
	 * 同意消息
	 */
	public static int MESSAGE_AGREE = 2;
	
	/**
	 * 课题组请求消息
	 */
	public static int MESSAGE_KTZREQ = 3;
	
	/**
	 * 拒绝消息
	 */
	public static int MESSAGE_REFUSE = -1;
	
	/**
	 * 回复类型：问答
	 */
	public static String Reply_Qa = "QA";
	
	/**
	 * 回复类型：博客
	 */
	public static String Reply_Blog = "BLOG";
}
