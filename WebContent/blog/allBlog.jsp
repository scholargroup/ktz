<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>js/jquery.thickbox/thickbox.css" type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.thickbox/thickbox-compressed.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
	});
	
	function follow(id) {
		var name = $("#f" + id).html();
		if(name == "关注"){
			$.get("qa_qaFollowAdd?id=" + id, function() {
				$("#f" + id).html("已关注");
			});
		}else if(name == "已关注"){
			$.get("qa_qaFollowCanel?id=" + id, function() {
				$("#f" + id).html("关注");
			});
		}
	}
</script>
	
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2" style="padding-top: 0px;">
			<div class="mainnav-2 clearfix">
				<div style="text-align: center">
					<img src="<%=CnLang.BASEPATH%>/img/icon/blogHead.png" alt="" />
				</div>
			</div>
			<table rules="cols" style="width: 100%; margin-top: 5px; margin-bottom: 5px; height: 300px;" >
				<tr>
					<td style="width: 100%;" class="titlelbl">
						<div class="left" <s:if test="1==#session.user_info.usrRole">style="display: none;"</s:if>> 
							<em class="gtgt">&gt;&gt;</em><a href="<%=CnLang.BASEPATH%>blog/add.html">发表你的博文</a>
						</div>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<s:if test="null==list||0==list.size()">
							<div class="norecord">
								暂时没有发表任何博客！
							</div>
						</s:if>
						<s:else>
							<div style="padding-left: 10px; padding-right: 10px;" class="overflow-y">
								<ul>
									<s:iterator value="list" id="blog" status="item">
										<li class="c-list-item">
											<table style="width: 100%; border-style: none;">
												<tr>
													<td style="width: 90px;" valign="top">
														<img alt="" src='<s:property value="#blog.usrAvatarUrl" />' style="width: 80px; height: 80px;"/>
													</td>
													
													<td>
														<div style="padding-left: 5px; padding-top: 3px;">
															<div style="margin-bottom: 3px;">
																<a href='<%=CnLang.BASEPATH%>user/<s:property value="#blog.usrId" />.html'><s:property value="#blog.usrName" /></a>：&nbsp;<s:property value="#blog.universityString" />&nbsp;<s:property value="#blog.universityString2" />
															</div>
															<div class="topics-post-feed-item-reason hiddenline" style="width: 700px;">
																<a style="font-weight: bold;" href="<%=CnLang.BASEPATH%>blog/info/<s:property value="#blog.id" />.html" title="<s:property value="#blog.title" />"><s:property value="#blog.title" /></a>
															</div>
															<div class="qa-topics-post-feed-item-title newline" style="text-indent: 2em; max-height: 70px;">
																<s:property value="#blog.content" />
															</div>
															<div class="item-title">
																<div class="left">评论数量：<span style="font-family: Georgia,Times New Roman,Times,serif;"><s:property value="#blog.answerCnt" /></span></div>
																<div class="right">
																	<a class="js-view-question btn btn-plain primary" href="<%=CnLang.BASEPATH%>blog/info/<s:property value="#blog.id" />.html">评论</a>
																</div>
															</div>
														</div>
													</td>
												</tr>
											</table>
										</li>
									</s:iterator>
								</ul>
								<%
									Object p = request.getAttribute("page");
									Object r = request.getAttribute("record");
									Object s = request.getAttribute("size");
									String u = CnLang.BASEPATH + "blog/allBlog";
									u = u + "/p@pageIndex.html";
									String url = "../view/page.jsp?p=" + p + "&r=" + r + "&s=" + s
											+ "&u=" + u;
								%>
								<jsp:include page="<%=url%>"></jsp:include>
							</div>
						</s:else>
					</td>
				</tr>
			</table>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>