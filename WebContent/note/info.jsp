<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css" type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/themes/default/default.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/plugins/code/prettify.css"
	type="text/css"></link>
<script type="text/javascript" src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#cancel").click(function() {
			var url ="../list.html";
			location.href = url;
		});
		
		var noteType = $("#noteType").val();
		if(noteType == 1){
			$("#qkText").html("期刊名：");
			$("#fileText").show();
			$("#txText").show();
			$("#txText2").show();
			$("#cbText").css("display","none");
			$("#yearPage").show();
		}else if(noteType == 2){
			$("#qkText").html("专利号：");
			$("#fileText").show();
			$("#txText").css("display","none");
			$("#txText2").css("display","none");
			$("#cbText").css("display","none");
			$("#yearPage").css("display","none");
		}else if(noteType == 3){
			$("#qkText").html("出版社：");
			$("#fileText").css("display","none");
			$("#txText").css("display","none");
			$("#txText2").css("display","none");
			$("#yearPage").css("display","none");
			$("#cbText").show();
		}
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>
						<s:if test="authorize==1">
							<a href="<%=CnLang.BASEPATH%>note/list.html">论文</a>
						</s:if>
						<s:else>
						<a href="<%=CnLang.BASEPATH%>note/<s:property value="noteInfo.usrId" />.html">论文</a>
						</s:else>
						<em class="gtgt">&gt;&gt;</em>详情
					</div>
				</div>
				<div class="items">
				<div class="info">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" colspan="2">
								<div class="title newline" style="width: 650px;">
									<s:property value="noteInfo.title" />
								</div>
							</td>
						</tr>
						<tr>							
							<td colspan="2">
								<table  style="border-bottom-style: none;" >
									<tr>
										<td style="width: 33%"><s:property value="noteInfo.at" /></td>
										<td style="text-align: center; width: 33%">
											<span style="font-weight: bold; color: red;">
												<s:if test="noteInfo.type==1">
													论文
												</s:if>
												<s:if test="noteInfo.type==2">
													专利
												</s:if>
												<s:if test="noteInfo.type==3">
													专著
												</s:if>
											</span>
											<input type="hidden" id="noteType" value="<s:property value="noteInfo.type" />"/>
										</td>
										<td style="text-align: right; width: 33%">
											<div class="hiddenline" style="width: 300px;"><span id="qkText" class="noteTitle">期刊名：</span><s:property value="noteInfo.periodical" /></div>
										</td>
									</tr>
									<tr style="border-bottom: none;">
										<td>
											<div id="yearPage">
												<span class="noteTitle">年卷期及页码：</span><span class="category"><s:property value="noteInfo.yearPage" /></span>
											</div>
										</td>
										<td colspan="2" style="text-align: right;">
											<span class="noteTitle">全部作者：</span><span class="category"><s:property value="noteInfo.firstAuthor" /></span>
											<span class="noteTitle" id="txText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;通讯作者：</span><span class="category" id="txText2"><s:property value="noteInfo.communAuthor" /></span>
											<span style="display: none" id="cbText">
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="noteTitle">出版时间：</span><fmt:formatDate value="${noteInfo.publishDate}" pattern="yyyy-MM-dd"/>
											</span>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr  style="display: none;">
							<td align="left" colspan="2">
								<div class="short newline" style="line-height: 24px; padding:6px 0px; min-height: 200px;">
									<s:property value="noteInfo.content" escapeHtml="false" />
								</div>
							</td>
						</tr>
						<tr>
							<td style="width: 80px;" id="fileText"><span style="font-weight: bold; color: red;">附件下载：</span></td>
							<td>
								<div id="fileList">
									<s:iterator value="listAttachment" id="file">
										<a class="post_file_down" title="<s:property value="#file.fileName" />" data-id="<s:property value="#file.id" />"
										href="fileUpload_downloadFile?id=<s:property value="#file.id" />"><s:property value="#file.shortFileName" escapeHtml="false" /></a>
									</s:iterator>
								</div>
							</td>
						</tr>
						<tr style="display:none">
							<td colspan="2"><input id="cancel" type="button" class="btn-90"
									value="返回"  />&nbsp;
							</td>
						</tr>
					</table>
					</div>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>