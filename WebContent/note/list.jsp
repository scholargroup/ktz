<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>/js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".item").hover(function() {
			$(this).find(".link").toggle();
		}, function() {
			$(this).find(".link").toggle();
		});
	});

	function del(id) {
		if (confirm("确定删除该记录？")) {
			$.get("note_del?id=" + id, function(data) {
				if (data.status == 1) {
					$("#n" + id).remove();
				}
			});
		}
	}
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>论文列表
						<s:if test="authorize==1">
							<a href="<%=CnLang.BASEPATH%>note/add.html" style="float: right;">+&nbsp;添加论文</a>
						</s:if>
					</div>
				</div>
				<div class="items">
				<s:if test="null==list||0==list.size()">
<div class="norecord">
	暂时没有发表任何论文！
</div>
</s:if>
<s:else>
					<s:iterator value="list" id="note">
						<div class="item" id="n<s:property value="#note.id" />">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<span class="title" style="font-weight: bold; color: red;">
											<s:if test="#note.type==1">
												论文
											</s:if>
											<s:if test="#note.type==2">
												专利
											</s:if>
											<s:if test="#note.type==3">
												专著
											</s:if>
										</span>
										&nbsp;&nbsp;
										<span >
											<a href="<%=CnLang.BASEPATH%>note/info/<s:property value="#note.id" />.html"><s:property value="#note.title" /></a> 
										</span>
										
									</td>
									<td style="width: 100px; text-align: right;"><s:property
											value="#note.at" /></td>
								</tr>
								<tr>
									<td>
										<span class="noteTitle">
											<s:if test="#note.type==1">
												期刊名：
											</s:if>
											<s:if test="#note.type==2">
												专利号：
											</s:if>
											<s:if test="#note.type==3">
												出版社：
											</s:if>
										</span>
										<span class="category"><s:property value="#note.periodical" /></span>
										<s:if test="#note.type==1">
											<span class="noteTitle" style="padding-left: 40px;">年卷期及页码：</span>
											<span><s:property value="#note.yearPage" /></span>
										</s:if>
									</td>
									<td style="text-align: right;">
										<s:if test="#note.listAttachment.size>0">
											<img title="附件" src="<%=CnLang.BASEPATH%>img/icon/attachment.png" />
										</s:if>
									</td>
								</tr>
								<tr>
									<td><span class="noteTitle">全部作者：</span><span class="category"><s:property
												value="#note.firstAuthor" /> </span>&nbsp;&nbsp;
										<s:if test="#note.type==1">
											<span class="noteTitle" style="padding-left: 20px;">通讯作者：</span><span class="category"><s:property
													value="#note.communAuthor" /></span>
										</s:if>
										<s:if test="#note.type==3">
											<span class="noteTitle" style="padding-left: 20px;">出版时间：</span><span class="category"><fmt:formatDate value="${note.publishDate}" pattern="yyyy-MM-dd"/></span>
										</s:if>
									</td>
									<td align="right"><span class="link" style="display: none;">
										<s:if test="authorize==1">
											<a href="javascript:del('<s:property value="#note.id" />')">删除</a>&nbsp;&nbsp;
											<a href="<%=CnLang.BASEPATH%>note/add/<s:property value="#note.id" />.html">编辑</a>&nbsp;&nbsp;
										</s:if>
											<a href="<%=CnLang.BASEPATH%>note/info/<s:property value="#note.id" />.html">详情</a> 
										</span>
									</td>
								</tr>
								<tr style="display: none;">
									<td align="left" colspan="2">
										<div class="short newline">
											<s:property value="#note.shortContent" escapeHtml="false" />
										</div>
									</td>
									<td></td>
								</tr>
							</table>
						</div>
					</s:iterator>
					<%
						Object p = request.getAttribute("page");
						Object r = request.getAttribute("record");
						Object s = request.getAttribute("size");
						Object id = request.getParameter("id");
						String u = CnLang.BASEPATH + "note/";
						if (null == id || id.equals("")) {
							u = u + "list";
						} else {
							u = u + id;
						}
						u = u + "/p@pageIndex.html";
						String url = "../view/page.jsp?p=" + p + "&r=" + r + "&s=" + s
								+ "&u=" + u;
					%>
					<jsp:include page="<%=url%>"></jsp:include>
					</s:else>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>