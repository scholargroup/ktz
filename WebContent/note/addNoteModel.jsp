<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/kindeditor-min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/lang/zh_CN.js"></script>
	
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>
	
<script type="text/javascript" src="<%=CnLang.BASEPATH%>js/base.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		init_editor();
		$('#txtTitle').val(parent.$('#note').val());
	});
	
	function cancel(){
		var r=confirm("是否确定取消?")
	    if (r==true)
	    {
	    	self.parent.tb_remove();
	    }
	}
</script>
</head>
<body>
		<!-- 正文内容 -->
		<div class="left w580">
			<div class="mainnav-2 clearfix">
				<div class="breadcrumb">
					发表论文
				</div>
			</div>
			<div class="overflow-y" style="height: 300px;">
				<div class="items " style="width: 98%">
					<form id="addnote" method="post" name="myform"
						action="<%=CnLang.BASEPATH%>note_modelSave"
						enctype="application/x-www-form-urlencoded">
						<table border="0" cellpadding="0" cellspacing="0"
							class="item-edit">
							<tr>
								<td class="w70">&nbsp;</td>
								<td>
									论文<input type="radio" name="noteInfo.type" value="1" checked="checked"/>&nbsp;&nbsp;
	                  					专利<input type="radio" name="noteInfo.type" value="2" />
	                  					<input type="hidden" name="noteInfo.id" />
	                  		  		</td>
							</tr>
							<tr>
								<td class="w70">标题：</td>
								<td><input id="txtTitle" name="noteInfo.title" class="txt-28 w370" maxlength="160" /></td>
							</tr>
							<tr>
								<td class="w70">全部作者：</td>
								<td>
									<input type="text" id="firstAuthor" name="noteInfo.firstAuthor" class="txt-28 w200" maxlength="80"/>&nbsp;&nbsp;
									通讯作者：<input type="text" id="communAuthor" name="noteInfo.communAuthor" class="txt-28 w90" maxlength="80"/>&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
							<tr>
								<td class="w70">期刊名：</td>
								<td><input id="periodical" name="noteInfo.periodical" class="txt-28 w370" maxlength="160" /></td>
							</tr>
							<tr style="display: none;">
								<td valign="top">内容：</td>
								<td style="padding:8px 0px;">
									<textarea id="content" name="noteInfo.content" style="height: 160px; width: 530px;"/>
								</td>
							</tr>
							<tr>
								<td valign="top">附件：</td>
								<td>
									<jsp:include page="/common/fileUpload.jsp">
										<jsp:param value="NoteEntity" name="paramEntityName"/>
										<jsp:param value="" name="paramEntityId"/>
									</jsp:include>
								</td>
							</tr>
					
						</table>
					</form>
				</div>
			</div>
			<div style="text-align: right">
				<input type="button" class="btn-90 mt8 mb8" value="保存" onclick="document.myform.submit()"/>&nbsp;&nbsp;
				<input type="button" class="btn-90 mt8 mb8" value="关闭" onclick="cancel()"/>
			</div>
		</div>
</body>
</html>