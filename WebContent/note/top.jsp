<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="mainnav-2 clearfix">
	<div class="breadcrumb left">
		<s:if test="authorize==1">
			<a href="<%=CnLang.BASEPATH%>note/add.html" style="float: right;">+&nbsp;添加论文专著</a>
		</s:if>
	</div>
	<div class="breadcrumb right">
		<a href="<%=CnLang.BASEPATH%>note/<s:property value="id" />.html" style="float: right;">查看更多<em
								class="gtgt">&gt;&gt;</em></a>
	</div>
</div>
<s:if test="null==list||0==list.size()">
<div class="norecord">
	暂时没有发表任何论文专著！
</div>
</s:if>
<s:else>
<div class="items">
	<s:iterator value="list" id="note">
		<div class="item" id="n<s:property value="#note.id" />">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<span class="title" style="font-weight: bold; color: red;">
							<s:if test="#note.type==1">
								论文
							</s:if>
							<s:if test="#note.type==2">
								专利
							</s:if>
							<s:if test="#note.type==3">
								专著
							</s:if>
						</span>
						&nbsp;&nbsp;
						<span>
							<a href="<%=CnLang.BASEPATH%>note/info/<s:property value="#note.id" />.html"><s:property value="#note.title" /> </a> 
						</span>
					</td>
					<td style="width: 100px; text-align: right;"><s:property
							value="#note.at" /></td>
				</tr>
				<tr>
					<td>
						<span class="noteTitle">
							<s:if test="#note.type==1">
								期刊名：
							</s:if>
							<s:if test="#note.type==2">
								专利号：
							</s:if>
							<s:if test="#note.type==3">
								出版社：
							</s:if>
						</span>
						<span class="category"><s:property value="#note.periodical" /></span>
						<s:if test="#note.type==1">
								<span class="noteTitle" style="padding-left: 40px;">年卷期及页码：</span>
								<span><s:property value="#note.yearPage" /></span>
						</s:if>
					</td>
					<td style="text-align: right;">
						<s:if test="#note.listAttachment.size>0">
							<img title="附件" src="<%=CnLang.BASEPATH%>img/icon/attachment.png" />
						</s:if>
					</td>
				</tr>
				<tr>
					<td><span class="noteTitle">全部作者：</span><span class="category"><s:property
								value="#note.firstAuthor" /></span>&nbsp;&nbsp;
						<s:if test="#note.type==1">
							<span class="noteTitle" style="padding-left: 20px;">通讯作者：</span><span class="category"><s:property
									value="#note.communAuthor" /></span>
						</s:if>
						<s:if test="#note.type==3">
							<span class="noteTitle" style="padding-left: 20px;">出版时间：</span><span class="category"><fmt:formatDate value="${note.publishDate}" pattern="yyyy-MM-dd"/></span>
						</s:if>
					</td>
					<td align="right"><span class="link" style="display: none;">							<a
							href="<%=CnLang.BASEPATH%>note/info/<s:property value="#note.id" />.html">详情
						</a> </span></td>
				</tr>
				<tr style="display: none;">
					<td align="left" colspan="2">
						<div class="short newline">
							<s:property value="#note.shortContent" escapeHtml="false" />
						</div>
					</td>
					<td></td>
				</tr>
			</table>
		</div>
	</s:iterator>
</div>
</s:else>