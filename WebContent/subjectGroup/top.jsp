<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
	
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		
		<div class="submain clearfix mt20 round-rect-2">
			<div class="mainnav-2 clearfix">
				<div class="breadcrumb">
				</div>
			</div>
			<table rules="cols" style="width: 100%; margin-top: 5px; margin-bottom: 5px;">
				<tr>
					<td style="width: 66%;" class="titlelbl">
						<div style="" > 
							<a href="<%=CnLang.BASEPATH%>subjectGroup/subjectList.html"><em class="gtgt">&gt;&gt;</em>查看更多课题组</a>
						</div>
					</td>
					<td style="width: 34%;" class="titlelbl">
						<div style="margin-left: 15px;" >
							<a href="<%=CnLang.BASEPATH%>subjectGroup/userList.html"><em class="gtgt">&gt;&gt;</em>查看更多导师和学生</a>
						</div>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<s:if test="null==subjectGroupList||0==subjectGroupList.size()">
							<div class="norecord">
								暂时没有课题组！
							</div>
						</s:if>
						<s:else>
							<div class="items overflow-y" style="padding-top: 10px; height: 800px;">
								<s:iterator value="subjectGroupList" id="user">
									<div class="usr-profile-l subject-div">
										<table>
											<tr>
												<td style="width: 100%; ">
													<table width="100%" border="0">
													<tr>
													<td style="width: 120px; ">
														<!-- <s:property value="#user.usrHdStringL" /> -->
														<img alt="" src="<s:property value="#user.usrHdStringM" />" style="width: 118px; height: 118px;"/>
													</td>
													<td valign="top">
													<div class="profile" style="margin-left: 5px;">
														<p style="line-height:20px; height:25px;">
															<span class="usr-name">
																<a href="<%=CnLang.BASEPATH%>user/<s:property value="#user.id" />.html"><s:property value="#user.usrName" /></a>
															</span>
															&nbsp;&nbsp;&nbsp;<span style="font-size: 20px; font-weight: bold; color: red;">课题组</span>
														</p>
														<p style="line-height:20px; height:25px;"><span  class="usr-us"><s:property value="#user.universityString" /></span></p>
														<p style="line-height:20px; height:25px;"><span  class="usr-us"><s:property value="#user.universityString2" /></span></p>
														<p style="line-height:20px; height:25px;"><span  class="usr-degree"><s:property value="#user.degreeString" />&nbsp;&nbsp;<s:property value="#user.titleString" /></span></p>
													</div></td>
													</tr>
													
													</table>
												</td>
											</tr>
											<tr>
												<td class="userDirectTd" valign="top">
													<div style="width: 100%;" class="newline">
														<s:property value="#user.researchDirect" />
													</div>
												</td>
											</tr>
										</table>
									</div>
								</s:iterator>
							</div>
						</s:else>
					</td>
					
					<td valign="top">
						<s:if test="null==hierophantList||0==hierophantList.size()">
							<div class="norecord">
								暂时没有导师和学生！
							</div>
						</s:if>
						<s:else>
							<div class="items overflow-y" style="padding-top: 10px; height: 800px;">
								<s:iterator value="hierophantList" id="user">
									<div style="margin-left: 5px;" class="usr-profile-l subject-div">
										<table>
											<tr>
												<td style="width: 100%;">
													<div style="width: 120px; float: left">
														<!-- <s:property value="#user.usrHdStringL" /> -->
														<img alt="" src="<s:property value="#user.usrHdStringM" />" style="width: 118px; height: 118px;"/>
													</div>
													<div class="profile" style="margin-left: 5px;">
														<p style="line-height:20px; height:25px;">
															<span class="usr-name">
																<a href="<%=CnLang.BASEPATH%>user/<s:property value="#user.id" />.html"><s:property value="#user.usrName" /></a>
															</span>&nbsp;
															<span ><s:property value="user.degreeString" />&nbsp;&nbsp;</span>
														</p>
														<p style="line-height:20px; height:25px;"><span  class="usr-us"><s:property value="#user.universityString" /></span></p>
														<p style="line-height:20px; height:25px;"><span  class="usr-us"><s:property value="#user.universityString2" /></span></p>
														<p style="line-height:20px; height:25px;"><span  class="usr-degree"><s:property value="#user.degreeString" />&nbsp;&nbsp;<s:property value="#user.titleString" /></span></p>
													</div>
												</td>
											</tr>
											<tr>
												<td class="userDirectTd" valign="top">
													<div style="width:100%;" class="newline">
														<s:property value="#user.researchDirect" />
													</div>
												</td>
											</tr>
										</table>
									</div>
								</s:iterator>
							</div>
						</s:else>
					</td>
				</tr>
			</table>
			
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>