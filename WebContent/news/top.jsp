<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

	<div class="mainnav-2 clearfix">
		<div class="breadcrumb left">
			<s:if test="authorize==1">
				<a href="<%=CnLang.BASEPATH%>news/add.html" style="float: right;">+&nbsp;添加新闻</a>
			</s:if>
		</div>
		<div class="breadcrumb right">
			<a href="<%=CnLang.BASEPATH%>news/<s:property value="id" />.html"
				style="float: right;">查看更多<em class="gtgt">&gt;&gt;</em></a>
		</div>
	</div>
	<s:if test="null==list||0==list.size()">
<div class="norecord">
	暂时没有发表任何新闻！
</div>
</s:if>
<s:else>
	<div class="items">
		<s:iterator value="list" id="item">
			<div class="item" id="n<s:property value="#item.id" />">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<span class="title">
								<a href="<%=CnLang.BASEPATH%>news/info/<s:property value="#item.id" />.html"><s:property value="#item.title" /></a>
							</span>
						</td>
						<td style="width: 100px; text-align: right;"><s:property
								value="#item.at" /></td>
					</tr>
				</table>
			</div>
		</s:iterator>
	</div>
</s:else>