<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="entity.UserEntity"%>
<%@page import="comm.CnLang"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	UserEntity userInfo = (UserEntity) session.getAttribute("user_info");
	String usrName = "";
	if (userInfo != null) {
		usrName = userInfo.getUsrName();
	}
	Object obj = session.getAttribute("user_info");
%>
<div class="head">
	<div class="main">
		<div style="width:1000px;" class="clearfix">
			<div class="logo"></div>
			<div class="clearfix" style="float: left;">
				<div class="menu" style="line-height: 30px;">
					<span style="font-family: 微软雅黑; font-size: 13px; padding: 0 6px">
						<span style="color: red;">S</span><span style="color: #fff;">cholar</span><span
						style="color: green">G</span><span style="color: #fff;">roup</span>
					</span>
					<%
						if (obj != null) {
					%>
					<a href="<%=CnLang.BASEPATH%>main.html"> <s:if
							test="#session.showMenu == 'main'">
							<font color="yellow">首页</font>
						</s:if> <s:else>首页</s:else>
					</a>&nbsp;&nbsp; <a href="<%=CnLang.BASEPATH%>qa.html"> <s:if
							test="#session.showMenu == 'qa'">
							<font color="yellow">问答</font>
						</s:if> <s:else>问答</s:else>
					</a>&nbsp;&nbsp; <a href="<%=CnLang.BASEPATH%>subjectGroup.html"> <s:if
							test="#session.showMenu == 'subjectGroup'">
							<font color="yellow">课题组</font>
						</s:if> <s:else>课题组</s:else>
					</a>&nbsp;&nbsp; <a href="<%=CnLang.BASEPATH%>job.html"> <s:if
							test="#session.showMenu == 'jobs'">
							<font color="yellow">招聘</font>
						</s:if> <s:else>招聘</s:else>
					</a>&nbsp;&nbsp;<a href="<%=CnLang.BASEPATH%>allBlog.html"> <s:if
							test="#session.showMenu == 'allBlog'">
							<font color="yellow">博客</font>
						</s:if> <s:else>博客</s:else>
					</a>&nbsp;&nbsp;
					<%
						}
					%>
				</div>

			</div>
			<div class="usr">
				<%
					if (obj == null) {
				%>
				<a href="<%=CnLang.BASEPATH%>login.html">登录</a>&nbsp;&nbsp;<a
					href="<%=CnLang.BASEPATH%>reg.html">注册</a>
				<%
					} else {
				%><span class="u-name">Hi&nbsp;,&nbsp;
					<a href="<%=CnLang.BASEPATH%>home.html"><s:property value="#session.user_info.usrName" /><s:if test="#session.user_info.usrRole == 1">的课题组</s:if></a>
					&nbsp;.&nbsp;&nbsp;
				 </span> 
				<a href="<%=CnLang.BASEPATH%>msg.html">消息</a>
				<%
					if (userInfo.getNewMsgNum() > 0) {
				%>
				(<a href="<%=CnLang.BASEPATH%>newMsg.html"><s:property value="#session.user_info.newMsgNum" /></a>)
				<%
					}
				%>
				&nbsp;&nbsp; 
				<a class="setting" title="设置" href="<%=CnLang.BASEPATH%>me.html">&nbsp;</a>&nbsp;&nbsp;
				<a href="<%=CnLang.BASEPATH%>logout.html">退出</a>
				<%
					}
				%>
			</div>
		</div>
	</div>
</div>
<div id="job" style="display: none;">
	<ul class="nav">
		<li><a href="<%=CnLang.BASEPATH%>job/add.html">招聘发布</a></li>
	</ul>
</div>
