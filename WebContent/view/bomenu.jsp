<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="entity.UserEntity"%>
<%@page import="comm.CnLang"%>
<%
	UserEntity userInfo = (UserEntity) session.getAttribute("user_info");
%>
<div class="bonav clearfix">
	<ul>
		<li class="usrItem"><a href="<%=CnLang.BASEPATH%>me.html" >个人资料<em class="gtgt green2"
				style="color:#619600;">&gt;&gt;</em> </a>
		</li>
		<li class="hpItem"><a href="<%=CnLang.BASEPATH%>hp.html">个人头像<em class="gtgt green2"
				style="color:#619600;">&gt;&gt;</em> </a>
		</li>
		<li class="imgItem"><a href="<%=CnLang.BASEPATH%>meqa.html" >我的提问<em class="gtgt green2"
				style="color:#619600;">&gt;&gt;</em> </a>
		</li>
		<li class="imgItem"><a href="<%=CnLang.BASEPATH%>meanswer.html" >我的回答<em class="gtgt green2"
				style="color:#619600;">&gt;&gt;</em> </a>
		</li>
		<li class="imgItem"><a href="<%=CnLang.BASEPATH%>mejob.html" >我的招聘<em class="gtgt green2"
				style="color:#619600;">&gt;&gt;</em> </a>
		</li>
		<%
			if (userInfo != null && 1 == userInfo.getUsrRole()) {
		%>
			<li class="imgItem"><a href="<%=CnLang.BASEPATH%>ktzManage.html" >课题组管理<em class="gtgt green2"
					style="color:#619600;">&gt;&gt;</em> </a>
			</li>
		<%
			}
		%>
		<li  class="pwdItem"><a href="<%=CnLang.BASEPATH%>pwd.html">修改密码<em class="gtgt green2"
				style="color:#619600;">&gt;&gt;</em> </a>
		</li>
		<li  class="pwdItem"><a href="<%=CnLang.BASEPATH%>msg.html">我的消息<em class="gtgt green2"
				style="color:#619600;">&gt;&gt;</em> </a>
		</li>
	</ul>
</div>
