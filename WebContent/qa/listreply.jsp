<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:if test="null==replyList||0==replyList.size()">
	<div class="norecord">暂时没有发表任何回复！</div>
</s:if>
<s:else>
	<div class="mainnav-2 clearfix">
	</div>
	<div class="items">
		<s:iterator value="replyList" id="reply">
			<div class="item" id="n<s:property value="#reply.id" />">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td rowspan="2" width="60px">
							<div class="img-s">
								<img alt="" src="<s:property value="#reply.usrAvatarUrl" />">
							</div>
						</td>
						<td>
							<span class="category"><a href="<%=CnLang.BASEPATH%>user/<s:property
								value="#reply.usrId" />.html" target="blank"><s:property value="#reply.usrName" /></a>
							</span>
						</td>
						<td style="width: 100px; text-align: right;">
							<s:property value="#reply.at" />
						</td>
					</tr>
					<tr>
						<td align="left" colspan="2">
							<div class="short" style="text-indent: 0em;">
								<s:property value="#reply.content" escapeHtml="false" />
							</div>
						</td>
						<td></td>
					</tr>
				</table>
			</div>
		</s:iterator>
		<%
			Object p = request.getAttribute("page");
				Object r = request.getAttribute("record");
				Object s = request.getAttribute("size");
				Object id = request.getParameter("id");
				String u = "javascript:more(@pageIndex,'" + id + "')";
				String url = "../view/page.jsp?p=" + p + "&r=" + r + "&s=" + s
						+ "&u=" + u;
		%>
		<jsp:include page="<%=url%>"></jsp:include>
	</div>
</s:else>
