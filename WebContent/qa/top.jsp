<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>js/jquery.thickbox/thickbox.css" type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.thickbox/thickbox-compressed.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#question").watermark("Q&A：提问你研究上的困惑获得你想要的答案.");
		tb_init("#xxxxxxx");
	});
	
	function follow(id) {
		var name = $("#f" + id).html();
		if(name == "关注"){
			$.get("qa_qaFollowAdd?id=" + id, function() {
				$("#f" + id).html("已关注");
			});
		}else if(name == "已关注"){
			$.get("qa_qaFollowCanel?id=" + id, function() {
				$("#f" + id).html("关注");
			});
		}
	}
</script>
	
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<div class="mainnav-2 clearfix">
				<div style="padding-left: 6px; height: 50px; line-height: 30px; font-family: 微软雅黑;">
					<input class="txt-28 w350" type="text" id="question"/>
					<a href="<%=CnLang.BASEPATH%>qa/addQaModel.jsp?height=450&width=600&modal=true" id="xxxxxxx"><input type="button" class="btn-90 mt8 mb8" value="提问" /></a>
				</div>
			</div>
			<table rules="cols" style="width: 100%; margin-top: 5px; margin-bottom: 5px; height: 300px;" >
				<tr style="height: 30px;">
					<td style="width: 100%;" class="titlelbl">
						<div class="left" > <em class="gtgt">&gt;&gt;</em>你可能回答的问题</div>
						<div class="right" style="padding-right: 10px;">
							<a href="<%=CnLang.BASEPATH%>qa/<s:property value="id" />.html" style="float: right;">
								<em class="gtgt">查看更多</em>
							</a>
						</div>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<s:if test="null==list||0==list.size()">
							<div class="norecord">
								暂时没有发表任何问答！
							</div>
						</s:if>
						<s:else>
							<div style="padding-left: 10px; padding-right: 10px;" class="overflow-y">
								<ul>
									<s:iterator value="list" id="qa" status="item">
										<li class="c-list-item">
											<table style="width: 100%; border-style: none;">
												<tr>
													<td style="width: 90px;" valign="top">
														<img alt="" src='<s:property value="#qa.usrAvatarUrl" />' style="width: 80px; height: 80px;"/>
													</td>
													
													<td>
														<div style="padding-left: 5px; padding-top: 3px;">
															<div style="margin-bottom: 3px;">
																<a href='<%=CnLang.BASEPATH%>user/<s:property value="#qa.usrId" />.html'><s:property value="#qa.usrName" /></a>：&nbsp;<s:property value="#qa.universityString" />&nbsp;<s:property value="#qa.universityString2" />
															</div>
															<div class="topics-post-feed-item-reason hiddenline" style="width: 700px;">
																<a style="font-weight: bold;" href="<%=CnLang.BASEPATH%>qa/info/<s:property value="#qa.id" />.html" title="<s:property value="#qa.title" />"><s:property value="#qa.title" /></a>
															</div>
															<div class="qa-topics-post-feed-item-title newline" style="text-indent: 2em; max-height: 70px;">
																<s:property value="#qa.content" />
															</div>
															<div class="item-title">
																<div class="left">回答：<span style="font-family: Georgia,Times New Roman,Times,serif;"> <s:property value="#qa.answerCnt" /></span></div>
																<div class="right">
																	<a class="js-view-question btn btn-plain primary" id="f<s:property value='#qa.id' />"  href="javascript:follow('<s:property value="#qa.id" />')"><s:if test="null==#qa.followId || 0==#qa.followId">关注</s:if><s:else>已关注</s:else></a>
																	<a class="js-view-question btn btn-plain primary" href="<%=CnLang.BASEPATH%>qa/info/<s:property value="#qa.id" />.html">回复</a>
																</div>
															</div>
														</div>
													</td>
												</tr>
											</table>
										</li>
									</s:iterator>
								</ul>
							</div>
						</s:else>
					</td>
				</tr>
			</table>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>