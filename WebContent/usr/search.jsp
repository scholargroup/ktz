<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>/css/com.css"
	type="text/css"></link>
<link href="<%=CnLang.BASEPATH%>js/jquery.thickbox/thickbox.css"
	rel="stylesheet" type="text/css" />

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.thickbox/thickbox-compressed.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".item").hover(function() {
			$(this).find(".link").toggle();
		}, function() {
			$(this).find(".link").toggle();
		});
	});

	function del(id) {
		if (confirm("确定关闭该用户？")) {
			$.get("user_close?id=" + id, function(data) {
				if (data.status == 1) {
					window.location.reload(true);
				}
			});
		}
	}

	function adduser(id) {
		//var url = "user/reset/" + id
		//		+ ".html?height=200&width=400&KeepThis=true&TB_iframe=true&t=";
		//tb_show("申请好友", url);
		// 目前不弹出消息框，直接申请
		$.get("message_addfriendMsg?id="+id,function(data){
			if(data.status==1){
				alert("发送申请成功！");
			}
			else{
				alert("发送申请失败！");
			}
		});
	}
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>搜索好友
					</div>
				</div>
				<div class="items">
					<s:iterator value="list" id="item">
						<div class="item">
							<table border="0" cellpadding="0" cellspacing="0"
								id="n<s:property value="#item.id" />">
								<tr>
									<td rowspan="2" width="60px" valign="top" align="left">
										<div class="img-s">
											<a
												href="<%=CnLang.BASEPATH%>user/info/<s:property value="#item.id" />.html"><img
												src="<s:property value="#item.usrHdString" />" /> </a>
										</div>
									</td>
									<td><span class="title2"><a
											href="<%=CnLang.BASEPATH%>user/info/<s:property value="#item.id" />.html"><s:property
													value="#item.usrName" /> </a> </span><s:if test="#item.sex==1">&nbsp;&nbsp;<span title="男">男</span>
										</s:if> <s:elseif test="#item.sex==0">&nbsp;&nbsp;<span
												title="女">女</span>
										</s:elseif> <s:else>&nbsp;&nbsp;<span title="保密"></span>
										</s:else></td>
									<td style="width: 130px; text-align: right;"><s:property
											value="#item.at" /></td>
								</tr>
								<tr>
									<td>大学：<b><s:property
													value="#item.universityString" />,<s:property
													value="#item.universityString2" escape="true"/></b>&nbsp;&nbsp;&nbsp;&nbsp;学历：<span
										class="category"><b><s:property
													value="#item.degreeString" /> </b> </span></td>
									<td align="right"><a
											href="javascript:adduser('<s:property
											value="#item.id" />')"><b>+&nbsp;加好友 </b></a></td>
								</tr>
							</table>
						</div>
					</s:iterator>
					<%
						Object p = request.getAttribute("page");
						Object r = request.getAttribute("record");
						Object s = request.getAttribute("size");
						String u = CnLang.BASEPATH + "user/search/p@pageIndex.html";
						String url = "../view/page.jsp?p=" + p + "&r=" + r + "&s=" + s
								+ "&u=" + u;
					%>
					<jsp:include page="<%=url%>"></jsp:include>
				</div>
			</div>
			<div class="right w210">
				<jsp:include page="../view/bomenu.jsp" flush="false" />
			</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>