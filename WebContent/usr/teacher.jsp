<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="usr-bonav clearfix" style="width: 99.5%">
	<h3>组内老师</h3>
	<ul>
		<s:iterator value="teacherList" id="user">
			<li class="clearfix">
				<div class="img-s">
					<img src="<s:property value="#user.usrHdStringM" />">
				</div>
				<p>
					<a href="<%=CnLang.BASEPATH%>user/<s:property value="#user.id" />.html"><s:property value="#user.usrName"/></a>
				</p>
				<p><s:property value="#user.degreeString"/>&nbsp;&nbsp;<s:property value="#user.titleString"/></p>
			</li>
		</s:iterator>
	</ul>
</div>