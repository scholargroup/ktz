<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.validform/validform.css"
	type="text/css"></link>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	var max = 0;
	$(document).ready(function() {
		var fObj = $(".items").find("li").eq(0);
		fObj.attr("class", "current");
		var id = fObj.attr("id");
		showSub(id);

		$(".items").find("li").click(function() {
			$(".items").find("li").removeAttr("class");
			var id = $(this).attr("id");
			$(this).attr("class", "current");
			//alert(id);
			showSub(id);
		})

		function showSub(id) {
			$(".sub").find("li").hide();
			id = "." + id;
			$(id).show();
		}

		$(".sub").find("li").click(function() {
			var cl = $(this).attr("class");
			if (cl.indexOf("current") > 0) {
				$(this).removeClass("current");
				setc();
			} else {
				if (max < 6) {
					$(this).addClass("current");
					setc();
				} else {
					alert("最多只能选择6个分类。");
				}
			}

		})

		function setc() {
			max =0;
			var ids = "";
			$(".sub").find("li").each(function() {
				var cl = $(this).attr("class");
				if (cl.indexOf("current")>0) {
					var id = $(this).attr("id");
					// setv(id);
					if(ids ==""){
						ids = id;
					}
					else{
						ids = ids+","+id;
					}
					max = max+1;
				}
			});
			$("#subtId").val(ids);
		}
		
		function setv(id){
			var ids = $("#subtId").val();
			if(ids=="" || ids=="0"){
			$("#subtId").val(id);
			}
			else{
				ids =ids+","+id;
				$("#subtId").val(ids);
			}
		}
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="reg">
				<form method="post" action="user_subject" class="registerform1">
					<div class="subject">
						<h3 class="title">
							<b>选择学科</b>
						</h3>
						<div class="items clearfix">
							<ul>
								<s:iterator value="list" id="item">
									<s:if test="#item.categoryId % 100==0">
										<li id="c<s:property value="#item.categoryId" />"><a
											href="javascript:void(0)"><s:property
													value="#item.categoryName" /></a></li>
									</s:if>
								</s:iterator>
							</ul>
						</div>
						<div class="sub clearfix">
							<ul>
								<s:iterator value="list" id="item">
									<s:if test="#item.categoryId % 100>0">
										<li class="c<s:property value="#item.categoryId/100*100" />"
											id="<s:property value="#item.categoryId" />"
											style="display: none;"><a href="javascript:void(0)"><s:property
													value="#item.categoryName" /></a></li>
									</s:if>
								</s:iterator>
							</ul>
						</div>
						<input type="hidden" name="subtId" value="0" id="subtId" />
						<table>
							<tr>
								<td><input type="submit" id="login" class="btn-90 mt6 w100"
									value="下一步" /></td>
								<td width="20px"></td>
								<td valign="bottom" style="display: none;">跳过设定&nbsp;&nbsp;<a
									href="<%=CnLang.BASEPATH%>home.html" style="font-weight: bold;">进入网站</a></td>
							</tr>
						</table>
						<p></p>
					</div>
				</form>
			</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>