<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.validform/validform.css"
	type="text/css"></link>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#vcode2").click(function() {
			$("#vcode2").attr("src", "view/vcode.jsp");
		});
		$("#uname").watermark("您的邮箱地址");
		$("#upwd").watermark("登录密码为6-16位");
		$("#upwd2").watermark("再输入一次登录密码.");
		$("#vcode").watermark("右侧的数字.");
		$(".myform").Validform({
			tiptype : 2,
			callback : function(form) {
				form[0].submit();
			}
		});
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w430">
				<div class="mlogin-title clearfix">
					<span class="mlogin">用户注册</span>
				</div>
				<s:if test="message!=null&&message!=''">
					<div class="message">
						<s:property value="message" />
					</div>
				</s:if>
				<div class="login">
					<form class="myform" method="post"
						action="<%=CnLang.BASEPATH%>user_step"
						enctype="application/x-www-form-urlencoded">
						<table border="0" cellpadding="0" cellspacing="0"
							class="tb-50 mt20">
							<tr>
								<td width="10px">&nbsp;</td>
								<td width="100px">我的姓名：</td>
								<td width="220px"><input id="uname" name="uname"
									value="<s:property value="uname" />" class="txtusr w200"
									maxlength="32" datatype="e" nullmsg=" " errormsg=" " /></td>
								<td width="100px" align="left"><div
										class="Validform_checktip"></div>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>我是：</td>
								<td><input type="radio"/>老师  <input type="radio"/>学生
								</td>
								<td align="left"><div class="Validform_checktip"></div>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td></td>
								<td align="left"><input type="submit"
									class="btn-90 mt6 w100" value="进入个人主页" />
								</td>
								<td></td>
							</tr>
						</table>
					</form>
				</div>
			</div>

		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>