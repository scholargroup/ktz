<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="mainnav-2 clearfix">
	<div class="breadcrumb">
		<a href="<%=CnLang.BASEPATH%>research/add/<s:if test="null==researchEntity">0</s:if><s:else><s:property value="researchEntity.id" /></s:else>.html" style="float: right;">
			<s:if test="authorize==1">
				编辑<em class="gtgt">&gt;&gt;</em>
			</s:if>
		</a>
	</div>
</div>
<s:if test="null==researchEntity||null==researchEntity.content||''==researchEntity.content">
	<div class="norecord">
		暂时没有发表研究方向！
	</div>
</s:if>
<s:else>
	<div class="items">
		<div class="item">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" style="text-align: center;">
						<div style="border-bottom: 1px dashed #009900; width: 80%; margin: auto;" class="hiddenline"><s:property value="researchEntity.title"/></div>
					</td>
				</tr>
				<tr style="height: 20px;">
					<td align="left">
					</td>
				</tr>
				<tr>
					<td align="left">
						<div class="short2 newline" style="max-width: 690px; overflow: auto;">
							<s:property value="researchEntity.content" escapeHtml="false" />
						</div>
					</td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>
</s:else>