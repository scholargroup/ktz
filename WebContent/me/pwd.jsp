<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.validform/validform.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.autocomplete/jquery.autocomplete.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.validform/Validform_v5.3.2.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.autocomplete/jquery.autocomplete.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#txtPwd").watermark("当前登录的密码.");
		$("#txtNPwd").watermark("新密码，密码长度6-16位.");
		$("#txtPwd2").watermark("同新密码一致.");
		$(".myform").Validform({
			tiptype : 2,
			callback : function(form) {
				form[0].submit();
			}
		});

		var sex = '<s:property value="userInfo.sex" />';
		$("#selSex").val(sex);
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em><a
							href="<%=CnLang.BASEPATH%>home.html">个人主页</a><em class="gtgt">&gt;&gt;</em>修改密码
					</div>
				</div>
				<s:if test="message!=null&&message!=''&&message==1">
					<div class="message-s">更新成功.</div>
				</s:if>
				<s:elseif test="message!=null&&message!=''">
					<div class="message-i">
						<s:property value="message" />
					</div>
				</s:elseif>
				<div class="items">
					<form method="post" action="<%=CnLang.BASEPATH%>pwd/save.html"
						enctype="application/x-www-form-urlencoded" class="myform">
						<table border="0" cellpadding="0" cellspacing="0"
							class="item-edit">
							<tr>
								<td class="w30"></td>
								<td class="w100">姓名：</td>
								<td class="w310"><input id="txtName"
									value="<s:property value="#session.user_info.usrName" />"
									class="txt-28 w300" maxlength="10" disabled="disabled" />
								</td>
								<td ><div class="Validform_checktip"></div>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>当前密码：</td>
								<td><input id="txtPwd" name="upwd" value=""
									class="txt-28 w300" maxlength="16" datatype="*" nullmsg=" "
									errormsg=" " type="password" /></td>
								<td align="left"><div class="Validform_checktip"></div>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>新密码：</td>
								<td><input id="txtNPwd" name="npwd" value=""
									class="txt-28 w300" maxlength="16" datatype="*6-16" nullmsg=" "
									errormsg="密码为6-16位字符." type="password" /></td>
								<td align="left"><div class="Validform_checktip"></div>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>确认密码：</td>
								<td class="w170"><input id="txtPwd2" name="upwd2" value=""
									class="txt-28 w300" maxlength="16" datatype="*" recheck="npwd"
									nullmsg="请再输入一次密码！" errormsg="两次输入密码不一致！" type="password" />
								</td>
								<td align="left"><div class="Validform_checktip"></div>
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" class="btn-90" value="更新" />&nbsp;</td>
								<td></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<div class="right w210">
				<jsp:include page="../view/bomenu.jsp" flush="false" />
			</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>