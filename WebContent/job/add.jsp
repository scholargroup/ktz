<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/kindeditor-min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/lang/zh_CN.js"></script>
	
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>
	
<script type="text/javascript" src="<%=CnLang.BASEPATH%>js/base.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		init_editorByName("info.jobRequire");
		$("#txtTitle").watermark("标题.");
		
		$.get("pub_jobTitle",function(data) {
			if (data.state == 1) {
				$("#selTitle option").remove();
				$("#selTitle").append("<option value='0'>未填</option>");
				$(data.list).each(function(i) {
					$("#selTitle").append(" <option value='" + data.list[i].categoryId + "'>"
											+ data.list[i].categoryName
											+ "</option>");
				});
				$("#selTitle").val('<s:property value="info.professTitle" />');
			}
		});
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em><a
							href="<%=CnLang.BASEPATH%>job/list.html">招聘信息</a><em class="gtgt">&gt;&gt;</em>添加
					</div>
				</div>
				<div class="items">
					<form id="addnote" method="post"
						action="<%=CnLang.BASEPATH%>job/save.html"
						enctype="application/x-www-form-urlencoded">
						<table border="0" cellpadding="0" cellspacing="0"
							class="item-edit">
							<tr>
								<td class="w70">标题：</td>
								<td><input id="txtTitle" name="info.title"
									value="<s:property value="info.title" />" class="txt-28 w300" maxlength="50" />
									<input type="hidden" name="info.id" value="<s:property value="info.id" />"/>
								</td>
							</tr>
							<tr>
								<td valign="top">招收对象：</td>
								<td>
									<select id="selTitle" class="combo-30 w110" name="info.professTitle">
									</select>
								</td>
							</tr>
							<tr>
								<td valign="top">学科方向：</td>
								<td style="padding:8px 0px;">
									<input name="info.jobDesc"
									  value="<s:property value="info.jobDesc" />" class="txt-28 w595" maxlength="100" />
								</td>
							</tr>
							<tr>
								<td valign="top">职位详情：</td>
								<td style="padding:8px 0px;"><textarea id="content2" name="info.jobRequire"
										style="visibility: hidden; ">
										<s:property value="info.jobRequire" />
									</textarea>
								</td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" class="btn-90 mt8 mb8" value="保存" />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>