<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.validform/validform.css"
	type="text/css"></link>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="reg">
			<div class="msg">
				<h3 class="title"><b>!</b> 感谢注册，请查收并验证邮件</h3>
				<div class="info">
					<p>
						我们已向&nbsp;<b><s:property value="ucode"/></b>&nbsp;发送验证邮件&nbsp;&nbsp;<a href="<s:property value="uname2"/>" target="_blank" style="font-weight: bold;text-decoration: none;font-family: Verdana;">点击跳转到登录邮箱</a>&nbsp;&nbsp;
					</p>
					<p>请登录邮箱点击确认链接即可激活帐号。</p>
					<p>也可以<a href="<%=CnLang.BASEPATH %>" style="font-weight: bold;">点击登录</a>，登录网站逛逛。</p>
				</div>
				<div class="tips">
					<p>没有收到邮件？</p>
					<ul>
						<li>到邮箱中的垃圾邮件、广告邮件目录中找找</li>
						<li>把邮箱输错了？<a href="<%=CnLang.BASEPATH %>reg.html">换个邮箱注册</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>