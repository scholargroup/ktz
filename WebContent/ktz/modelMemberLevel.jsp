<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/kindeditor-min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/lang/zh_CN.js"></script>
	
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>
	
<script type="text/javascript" src="<%=CnLang.BASEPATH%>js/base.js"></script>

<script type="text/javascript">
	var userId = "";
	var headManFlag = "";
	$(document).ready(function() {
		userId = parent.$("#userId").val();
		headManFlag = parent.$("#userHeadManFlag").val();
		
		$.get("pub_memberLevel",function(data) {
			$("#selLevel option").remove();
			$(data.list).each(function(i) {
				$("#selLevel").append(" <option value='" + data.list[i].categoryId + "'>"
										+ data.list[i].categoryName
										+ "</option>");
			});
			$("#selLevel").val(headManFlag);
		});
	});
	
	function setMemberLevel(){
		var levelId = $("#selLevel").val();
		if(levelId != headManFlag){
			var url = "user_setKtzMemberLevel?id=" + userId + "&memberLevel=" + levelId;
			$.get(url, function(data) {
				if (data.status == 1) {
					alert("修改成功");
					parent.window.location.reload(true);
					//parent.document.myform.submit();
				}else{
					alert(data.info);
				}
			});
		}else{
			self.parent.tb_remove();
		}
	}
	
	function cancel(){
		self.parent.tb_remove();
	}
</script>
</head>
<body>
		<!-- 正文内容 -->
		<div style="width: 100%">
			<div class="items">
				<table align="center" class="item-edit" border="0" cellpadding="0" cellspacing="0">
			        <tr>
			            <td>
			           	     课题组身份
			           	</td>
			            <td>
			  		        <select class="combo-30 w110" id="selLevel" >		
							</select>
						</td>
			        </tr>
			        <tr>
			            <td>
			            </td>
			            <td>
			            	<input type="button" class="btn-90 mt8 mb8" value="确定" onclick="setMemberLevel()"/>
			            	<input type="button" class="btn-90 mt8 mb8" value="关闭" onclick="cancel()"/>
						</td>
			        </tr>
		       </table>
			</div>
		</div>
</body>
</html>