<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/kindeditor-min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/lang/zh_CN.js"></script>
	
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>
	
<script type="text/javascript" src="<%=CnLang.BASEPATH%>js/base.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		memberList(0);
	});
	
	function memberList(page) {
		var userName = $("#userName").val();
		var univName = $("#univName").val();
		var url = "user_member?t="+Math.random();
		var data = "uname=" + userName + "&univName=" + univName;
		if(page != 0){
			data = data + "&p=" + page;
		}
		$.post(url, data, function(data) {
			if (data.state == 1) {
				$("#member").html("");
				var tableHtml = ("<tr style='text-align: center;' class='userTableRow1'>"
					            + "<td width='6%'>序号 </td>"
					            + "<td width='12%'>姓名 </td>"
					            + "<td width='10%'>性别 </td>"
					            + "<td width='18%'>职称 </td>"
					            + "<td width='40%'>大学院系</td>"
					            + "<td>操作</td>"
					        + "</tr>");
				var j = 0;
				$(data.list).each(function(i) {
					var user = data.list[i];
					j = i + 1;
					var classPro = "";
					if(i%2==1){
						classPro = "userTableRow1";
					}else{
						classPro = "userTableRow2";
					}
					tableHtml = tableHtml + "<tr class='" + classPro + "'>"
						         + "<td style='text-align: center;'>" + j + "</td>"
						         + "<td><span class='usr-name'><a href='<%=CnLang.BASEPATH%>user/" + user.id + ".html' target='blank'>" + user.usrName + "</a></span></td>"
						         + "<td>" + user.sexString + "</td>"
						         + "<td>" + user.degreeString + "</td>"
						         + "<td>" + user.universityString + "</td>"
						         + "<td><a href='javascript:addMember(" + user.id + ")'>添加</a></td>"
					         	 + "</tr>";
				});
				//保证table有10行，不够添加空行
				j = j + 1
				for( var i = j; i <= 10; i++){
					var classPro = "";
					if(i%2==1){
						classPro = "userTableRow1";
					}else{
						classPro = "userTableRow2";
					}
					tableHtml = tableHtml + "<tr class='" + classPro + "'>"
						         + "<td></td>"
						         + "<td></td>"
						         + "<td></td>"
						         + "<td></td>"
						         + "<td></td>"
						         + "<td></td>"
					         	 + "</tr>";
				}
				$("#member").html(tableHtml);
				if(data.html != null){
					$("#page").html(data.html);
				}else{
					$("#page").html("");
				}
			}
		});
	}
	
	function addMember(id){
		var url = "message_ktzAddMemberMsg?id=" + id;
		$.get(url, function(data) {
			if (data.status == 1) {
				alert("发送要求成功");
			}else{
				alert("发送要求失败");
			}
		});
	}
	
	function cancel(){
		self.parent.tb_remove();
	}
</script>
</head>
<body>
		<!-- 正文内容 -->
		<div style="width: 100%">
			<div class="mainnav-2 clearfix" style="display: none;">
				<div class="breadcrumb">
					---添加课题组成员--
				</div>
			</div>
			<div class="items">
				<table class="userTableBorder  tableText" style="border-collapse: separate; border-spacing: 1px; width: 100%" align="center" border="0">
			        <tr>
			            <td class="userTableRow2" width="30%">
			           	     姓名：<input type="text" id="userName"/>
			           	</td>
			               
			            <td class="userTableRow2" width="33%">
				  		      大学名称:<input type="text" id="univName"/></td>
				        <td class="userTableRow2" width="20%">
							<input type="button" value="搜索" onclick="memberList(0)"/>
				        </td>
			        </tr>
			        <tr style="height: 10px">
			            <td class="userTableRow1">
			                </td>
			            <td class="userTableRow1">
			              </td>
			            <td class="userTableRow1" colspan="2">
			                </td>
			        </tr>
		       </table>
					       
				<table id="member" class="userTableBorder tableText" style=" border-collapse: separate; border-spacing: 1px; width: 100%" align="center"
					        border="0">
					  <tr style="text-align: center;" class="userTableRow1">
			            <td width="6%">
			                                         序号	
			            </td>
			            <td width="12%">
			      		          姓名
			            </td>
			            <td width="10%">
			      		          性别
			            </td>
			            <td width="18%">
			               	职称
			            </td>
			            <td width="40%">
			               	大学院系
			            </td>
			            <td>
			        		     操作 
			            </td>
			         </tr>
					 <s:iterator value="{1,2,3,4,5,6,7,8,9,10}" status="item">
					 	<tr <s:if test="#item.count%2==1">class="userTableRow2"</s:if>
				              <s:if test="#item.count%2==0">class="userTableRow1"</s:if>
				           >
					            <td style="text-align: center;">
					            </td>
					            <td>
					            </td>
					            <td>
					            </td>
					            <td>
					            </td>
					            <td>
					            </td>
					            <td>
					            </td>
				          </tr>
					 </s:iterator>
				</table>
			</div>
			<div id="page" style="margin-bottom: 30px;"></div>
			<br/>
			<div style="text-align: right;">
				<input type="button" class="btn-90 mt8 mb8" value="关闭" onclick="cancel()"/>
			</div>
		</div>
</body>
</html>