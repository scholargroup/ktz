<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/kindeditor.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/lang/zh_CN.js"></script>
	
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>
	
<script type="text/javascript" src="<%=CnLang.BASEPATH%>js/base.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		init_editor();
		$("#content").watermark("简历的内容.");
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em><a
							href="#">个人简历</a><em class="gtgt">&gt;&gt;</em>编辑
					</div>
				</div>
				<div class="items">
					<form id="addresume" method="post"
						action="<%=CnLang.BASEPATH%>resume_save"
						enctype="application/x-www-form-urlencoded">
						<table border="0" cellpadding="0" cellspacing="0"
							class="item-edit">
							<tr>
								<td valign="top">内容：</td>
								<td style="padding:8px 0px;"><textarea id="content" name="resumeEntity.content"
										style="visibility: hidden; ">
										<s:property value="resumeEntity.content" />
									</textarea>
									<input type="hidden" name="resumeEntity.id" value="<s:property value="resumeEntity.id" />"/>
								</td>
							</tr>
							<tr>
								<td><br /><br /></td>
								<td><input type="submit" class="btn-90 mt8 mb8" value="保存" />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>